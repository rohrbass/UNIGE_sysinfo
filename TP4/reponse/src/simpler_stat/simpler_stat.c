#include "simpler_stat.h"
#include "../lstat.h"
#include <errno.h>
#include <string.h>


Simple_Stat simple_stat( const string path ) {

	Simple_Stat s;
	s.status = 0;

	// ONCE WITH LSTAT
	Stat infos = {0};
	if( lstat( path, &infos ) < 0 ) {
		if( errno & ENOENT ) {
			s.status |= SIMPLE_STAT_STATUS_NOT_EXISTS;
		} else {
			s.status |= SIMPLE_STAT_STATUS_ERROR;
		}
		// char *err = strerror(errno);
		// fprintf( stderr, "Cannot stat %s: %s\n", src, err );
	} else {
		if( S_ISLNK( infos.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_SYMLINK;
		}
		if( S_ISDIR( infos.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_DIRECTORY;
		}
		if( S_ISREG( infos.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_FILE;
		}
	}


	// ONCE WITH STAT
	Stat infos2 = {0};
	if( stat( path, &infos2 ) < 0 ) {
		if( errno & ENOENT ) {
			s.status |= SIMPLE_STAT_STATUS_NOT_EXISTS;
		} else {
			s.status |= SIMPLE_STAT_STATUS_ERROR;
		}
		// char *err = strerror(errno);
		// fprintf( stderr, "Cannot stat %s: %s\n", src, err );
	} else {
		if( S_ISLNK( infos2.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_SYMLINK;
		}
		if( S_ISDIR( infos2.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_DIRECTORY;
		}
		if( S_ISREG( infos2.st_mode ) ) {
			s.status |= SIMPLE_STAT_STATUS_FILE;
		}
	}

	s.time = infos2.st_mtime;
	s.size = infos2.st_size;
	s.mode = infos2.st_mode;

	return s;

}

// those return true or false (not weird numbers)
bool stat_error(Simple_Stat s){
	if( s.status & SIMPLE_STAT_STATUS_ERROR ) {
		return true;
	}
	return false;
}
bool stat_not_exist(Simple_Stat s){
	if( s.status & SIMPLE_STAT_STATUS_NOT_EXISTS ) {
		return true;
	}
	return false;
}
bool stat_file(Simple_Stat s){
	if( s.status & SIMPLE_STAT_STATUS_FILE ) {
		return true;
	}
	return false;
}
bool stat_directory(Simple_Stat s){
	if( s.status & SIMPLE_STAT_STATUS_DIRECTORY ) {
		return true;
	}
	return false;
}
bool stat_symlink(Simple_Stat s){
	if( s.status & SIMPLE_STAT_STATUS_SYMLINK ) {
		return true;
	}
	return false;
}

string stat_error_string(Simple_Stat s) {
	return strerror( s.error_number );
}
