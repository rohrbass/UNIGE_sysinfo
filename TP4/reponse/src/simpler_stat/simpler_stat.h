#pragma once
#include "../types.h"

#define SIMPLE_STAT_STATUS_ERROR 1<<0
#define SIMPLE_STAT_STATUS_NOT_EXISTS 1<<1
#define SIMPLE_STAT_STATUS_FILE 1<<2
#define SIMPLE_STAT_STATUS_DIRECTORY 1<<3
#define SIMPLE_STAT_STATUS_SYMLINK 1<<4

typedef struct {
	int status;
	int error_number;
	time_t time;
	off_t  size;
	mode_t mode;
} Simple_Stat;

Simple_Stat simple_stat( const string path );

bool stat_error(Simple_Stat s);
string stat_error_string(Simple_Stat s);
bool stat_not_exist(Simple_Stat s);
bool stat_file(Simple_Stat s);
bool stat_directory(Simple_Stat s);
bool stat_symlink(Simple_Stat s);
