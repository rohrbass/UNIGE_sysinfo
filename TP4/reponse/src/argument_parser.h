#pragma once

#include <unistd.h>

#include "types.h"

#define ACTION_LS 1<<0
#define ACTION_RSYNC 1<<1
#define ACTION_SYMLINK_SPECIAL_TREATMENT 1<<2
#define ACTION_FAIL_ON_ERROR 1<<3
#define ACTION_CHMOD_ANYWAYS 1<<4

typedef struct {
	Array(string) arguments_no_option;
	int action;
} What_to_do_type;
typedef What_to_do_type* What_to_do;

What_to_do new_WTD();
void delete_WTD( What_to_do* wtd );

void print_help( string binary_name);
What_to_do argument_parse(int argc, char* argv[]);
