#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include <errno.h>
#include <string.h>

#include "any_arrays.h"
#include "types.h"
#include "utils.h"
#include "ls/ls.h"
#include "cp/smart_cp.h"
#include "lstat.h"
#include "argument_parser.h"
#include "simpler_stat/simpler_stat.h"
#include "cp/cp.h"

allow_array_map_void( string );

prototype_array_of( int );
implement_array_of( int );
allow_array_map_void( int );

int main(int argc, char* argv[]) {

	What_to_do wtd = argument_parse( argc, argv );

	if( wtd->action & ACTION_LS ) {
		ls( array_get(wtd->arguments_no_option, 0 ) );
	}

	if( wtd->action & ACTION_RSYNC ) {
		string dest = array_pop(wtd->arguments_no_option);

		// ./ultra-cp file file => copy file to dest
		// ./ultra-cp file absent => copy file to dest
		// ./ultra-cp file file file absent => ERROR

		// Question: dans quel cas les droits ne pourrons pas être les mêmes ?
		// Réponse : dans le cas où on essaye d'assigner plus de droits qu'on en a sur la destination.

		// --- check src and dest ---
		Simple_Stat dest_stat = simple_stat(dest);
		if( stat_error( dest_stat ) ) {
			fprintf( stderr, "ERROR_026 : %s\n", stat_error_string( dest_stat ) );
			exit(EXIT_FAILURE);
		}

		// if
		//   - only one file
		//   - dest is a file (or does not exist)
		if( ( array_size( wtd->arguments_no_option ) == 1 )
			&&
			( stat_not_exist( dest_stat ) || stat_file( dest_stat ) ) )
		{
			string src = array_get( wtd->arguments_no_option, 0 );
			Simple_Stat src_stat = simple_stat( src );

			if( stat_error( src_stat ) ) {
				fprintf( stderr, "ERROR_027 %s\n", stat_error_string( src_stat ) );
				exit(EXIT_FAILURE);
			}
			
			// if the entry is a file, then do that special thing
			if( stat_file( src_stat ) ) {
				if( cp_file( src, dest, wtd->action & ACTION_CHMOD_ANYWAYS ) != 0 ) {
					if(wtd->action & ACTION_FAIL_ON_ERROR) { exit( EXIT_FAILURE ); }
				}
				exit( EXIT_SUCCESS );
			}
			// otherwise go back to regular stuff
		}

		// if( stat_not_exist( dest_stat ) ) {
		// 	// mkdir 775
		// 	if( mkdir( dest, S_IRWXU | S_IRWXG | S_IXOTH | S_IROTH ) != 0 ) {
		// 		fprintf( stderr, "ERROR : could not mkdir \"%s\"\n", dest );
		// 		exit(EXIT_FAILURE);
		// 	}
		// }

		// if dest is messed up, smart_cp with handle that scenario

		// --- start the loop ---
		void smart_cp_callback(string src, size_t index, Array( string ) array) {
			UNUSED( array ); UNUSED( index );

			string absolute_src = realpath( src, NULL );

			// si dest n'existe pas, on va fail ici, comme décrit dans l'énoncé :
			string absolute_dest = realpath( dest, NULL );
			if( absolute_dest == NULL ) {
				if( stat_symlink( dest_stat ) ) {
					fprintf( stderr, "ERROR_998 : \"%s\" destination is a broken symlink\n", dest );
					exit(EXIT_FAILURE);
				} else {
					fprintf( stderr, "ERROR_999 : \"%s\" does not exist\n", dest );
					exit(EXIT_FAILURE);
				}
			}

			smart_cp( absolute_src, absolute_dest, wtd->action & ACTION_SYMLINK_SPECIAL_TREATMENT, wtd->action & ACTION_FAIL_ON_ERROR, wtd->action & ACTION_CHMOD_ANYWAYS );

			free(absolute_dest);
			free(absolute_src);
		}
		array_map_void(string)( wtd->arguments_no_option, smart_cp_callback );
	}

	delete_WTD(&wtd);
	exit(EXIT_SUCCESS);
}
