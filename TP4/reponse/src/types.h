#pragma once
#include <sys/stat.h>

#include "any_arrays.h"

typedef char* string;
typedef struct stat Stat;

prototype_array_of( string );

typedef enum { false = 0, true = 1 } bool;
