#include "string.h"

string new_string( size_t length ) {
	string out = malloc( ( length + 1 ) * sizeof( char ) );
	out[0] = '\0';
	return out;
}

string string_concatenate( string left, string right ) {
	string out = new_string( strlen(left) + strlen(right) );
	out[0]='\0';
	out = strcat(out, left);
	out = strcat(out, right);
	return out;
}

string string_concatenate2( string left, string middle, string right ) {
	string tmp = string_concatenate( middle, right );
	string out = string_concatenate( left, tmp );
	free(tmp);
	return out;
}
