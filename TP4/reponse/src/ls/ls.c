#include "ls.h"
#include "../string.h"

#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <dirent.h>
#include "../simpler_stat/simpler_stat.h"

string time_to_str( time_t t ) {
	size_t DATE_MAX_LENGTH = 1024;
	string date = new_string( DATE_MAX_LENGTH );

	struct tm *tmp = localtime(&t);
	if (tmp == NULL) {
		perror("ERROR_025 : localtime\n");
		exit(EXIT_FAILURE);
	}

	if ( strftime(date, DATE_MAX_LENGTH, "%a %b %d %T %Y", tmp) == 0 ) {
		fprintf(stderr, "ERROR_023 : strftime failed\n");
		exit(EXIT_FAILURE);
	}

	return date;
}

string mode_to_str( mode_t mode ) {

	// allocate 10+1 chars
	string out = new_string(10);

	// --- TYPE ---
	out[0] =
		S_ISREG(mode) ? '-' :
		S_ISLNK(mode) ? 'l' :
		S_ISDIR(mode) ? 'd' :
		S_ISFIFO(mode) ? 'p' :
		// S_ISSOCK(stat.mode) ? 's' : // in man page but does not exist ?
		S_ISBLK(mode) ? 'b' :
		S_ISCHR(mode) ? 'c' :
		'?';

	// --- RIGHTS ---
	out[1] = mode & S_IRUSR ? 'r' : '-';
	out[2] = mode & S_IWUSR ? 'w' : '-';
	out[3] = mode & S_IXUSR ? 'x' : '-';

	out[4] = mode & S_IRGRP ? 'r' : '-';
	out[5] = mode & S_IWGRP ? 'w' : '-';
	out[6] = mode & S_IXGRP ? 'x' : '-';

	out[7] = mode & S_IROTH ? 'r' : '-';
	out[8] = mode & S_IWOTH ? 'w' : '-';
	out[9] = mode & S_IXOTH ? 'x' : '-';

	out[10] = '\0';

	return out;
}

void print_stat( Simple_Stat stat, string path ) {

	// --- MODE ---
	string mode = mode_to_str( stat.mode );

	// --- SIZE ---
	long int size = stat.size;

	// --- DATE ---
	string date = time_to_str( stat.time );

	// --- print ---
	printf( "%s %10li  %s   %s\n" , mode, size, date,  path);

	free(date);
	free(mode);

}

void ls( string path ) {

	// TODO : use Simple_Stat
	Simple_Stat stat = simple_stat( path );

	if( stat_error( stat ) ) {
		fprintf( stderr, "ERROR_024 : Cannot stat %s: %s\n", path, stat_error_string( stat ) );
		// TODO : maybe add an option to determine whether this should crash or not
		// exit(EXIT_FAILURE);
		// for now the file is skipped
		return;
	}

	print_stat( stat, path );

	// if is a directory, do the same stuff on each sub-file/dir
	// int is_directory = S_ISDIR( infos_ignore_link.st_mode );
	// int is_symlink = S_ISLNK( infos.st_mode );
	if ( stat_directory( stat ) ) {

		if( stat_symlink( stat ) ) {
			// TODO :
			// decide what to do with symlinks
		}

		DIR *d = opendir( path );
		if( d ) {
			struct dirent *dir;
			while ((dir = readdir(d)) != NULL) {
				if( strcmp(dir->d_name, "." ) == 0 || strcmp(dir->d_name, ".." ) == 0 ) {
					// ignore . and ..
					continue;
				}
				string new_path; 
				if(*(path + strlen(path) -1)=='/')
				{
					new_path = string_concatenate( path, dir->d_name); 
				}
				else
				{
					new_path = string_concatenate2( path, "/", dir->d_name );
				}

				ls( new_path );
				free(new_path);
			}
			closedir( d );
		} else {
			fprintf( stderr, "ERROR_21 : could not open \"%s\"\n", path );
		}
	}
}
