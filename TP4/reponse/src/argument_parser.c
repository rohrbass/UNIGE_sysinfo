#include "argument_parser.h"
#include <getopt.h>

What_to_do new_WTD() {
	What_to_do wtd = (What_to_do) malloc( sizeof( What_to_do ) );
	wtd->arguments_no_option = new_array(string);
	wtd->action = 0;
	return wtd;
}

void delete_WTD( What_to_do* wtd ) {
	array_free( (*wtd)->arguments_no_option );
	*wtd = NULL;
}


void print_help( string binary_name) {
	printf(
	"usage : \n"
	"\n"
	"  to list files with their properties (recursive)\n"
	"    $ %s [-h] path\n"
	"\n"
	"  to backup files smartly\n"
	"    $ %s [-h] [-f] [-e] source_path [source_path_2 ...] dest_path\n"
	"\n"
	"Option :\n"
	"  -h : prints this message\n"
	"  -f : symlinks are not recursively copied and transformed to absolute paths in destination\n"
	"  -F : disable [-f]\n"
	"  -e : fail on error\n"
	"  -E : continue on error (default)\n"
	"  -a : always chmod\n"
	, binary_name,binary_name);
}

What_to_do argument_parse(int argc, char* argv[]) {

	What_to_do wtd = new_WTD();

	int opt;
	while( ( opt = getopt(argc, argv, "-hfea") ) != -1 ) {
		switch( opt ) {
			case 'h' :
				print_help(argv[0]);
				exit(EXIT_SUCCESS);
				break;
			case 'f' :
				wtd->action |= ACTION_SYMLINK_SPECIAL_TREATMENT;
				break;
			case 'F' :
				wtd->action &= ~(ACTION_SYMLINK_SPECIAL_TREATMENT);
				break;
			case 'e' :
				wtd->action |= ACTION_FAIL_ON_ERROR;
				break;
			case 'E' :
				wtd->action &= ~(ACTION_FAIL_ON_ERROR);
				break;
			case 'a' :
				wtd->action |= ACTION_CHMOD_ANYWAYS;
				break;
			default:
				array_push( wtd->arguments_no_option, optarg );
				break;
		}
	}

	switch ( array_size( wtd->arguments_no_option ) ) {
		// si 0 argument, on prévoit de faire ls
		case 0:
			print_help(argv[0]);
			exit(EXIT_FAILURE);
			break;
		// si 1 argument, on prévoit de faire ls
		case 1:
			wtd->action |= ACTION_LS;
			break;
		// si n arguments, on prévoit de faire ls
		default:
			wtd->action |= ACTION_RSYNC;
			break;
	}

	return wtd;
}
