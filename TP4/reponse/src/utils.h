#pragma once

// removes annoying compilation warning
#define UNUSED(x) (void)(x)
#define MACRO_FORCE_SEMICOLON void MACRO_FORCE_SEMICOLON_PLAY_WITH_C_SYNTAX()
