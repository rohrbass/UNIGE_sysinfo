#include "cp.h"
#include <errno.h>

#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "../simpler_stat/simpler_stat.h"

int close_print_error( int fd, const string path ) {
	if( close( fd ) < 0 ) {
		fprintf( stderr, "ERROR_022 : Could not close the file %s: %s\n", path, strerror( errno ) );
		return -1;
	}
	return 0;
}

int cp_file( const string src, const string dest, bool always_chmod ) {

	// is the copy necessary ?
	Simple_Stat src_stat = simple_stat(src);
	Simple_Stat dest_stat = simple_stat(dest);

	if( stat_error( src_stat ) ) {
		fprintf( stderr, "ERROR_009 : %s\n", stat_error_string( src_stat ) );
		return -1;
	}

	if( stat_error( dest_stat ) ) {
		fprintf( stderr, "ERROR_010 : %s\n", stat_error_string( dest_stat ) );
		return -1;
	}

	// skip copy criteria
	if( (src_stat.size == dest_stat.size) && (src_stat.time < dest_stat.time) ) {
		printf("already up to date : %s\n", dest);
		// just update mode
		if( always_chmod ) {
			chmod(dest,src_stat.mode);
		}
		return 0;
	}

	// ... yeah we need to copy

	// open, read only
	int fd_src = open(src, O_RDONLY);
	if ( fd_src < 0 ) {
		fprintf( stderr, "ERROR_011 : Could not open the file %s: %s\n", src, strerror( errno ) );
		return -1;
	}

	if( !stat_not_exist( dest_stat ) ) {
		if (remove( dest ) != 0) {
			fprintf( stderr, "ERROR_012 : Unable to delete %s\n", dest ); 
		}
	}

	int fd_dest = open( dest, O_WRONLY | O_CREAT , src_stat.mode );
	if ( fd_dest < 0 ) {
		int savedError = errno;
		close( fd_src );
		fprintf( stderr, "ERROR_013 : Could not open the file %s: %s\n", dest, strerror( savedError ) );
		return -1;
	}

	char buf[4096];
	ssize_t n_read;
	while( n_read = read( fd_src, buf, sizeof buf ), n_read > 0 ) {
		char *out_ptr = buf;
		do {
			ssize_t nwritten = write(fd_dest, out_ptr, n_read);

			if ( nwritten >= 0 ) {
				n_read -= nwritten;
				out_ptr += nwritten;
			} else if ( errno != EINTR ) {
				int savedError =errno;
				close_print_error( fd_src  , src );
				close_print_error( fd_dest , dest );
				fprintf( stderr, "ERROR_014 : Could not copy  %s to %s: %s\n", src, dest, strerror(savedError) );
				return -1;
			}

		} while (n_read > 0);
	}

	if (n_read != 0) {
		int savedError =errno;
		close_print_error( fd_src , src );
		close_print_error( fd_dest, dest );
		fprintf( stderr, "ERROR_015 : Could not read %s: %s\n", src, strerror(savedError) );
		return -1;
	}

	int ret = 0;
	if( close_print_error( fd_src, src ) < 0 ) {
		// TODO : ret = -1 ?
	}
	if( close_print_error( fd_dest, dest ) < 0 ) {
		// TODO : ret = -1 ?
	}
	return ret;
}

int cp_symlink( const string src, const string dest, bool always_chmod ) {
	Simple_Stat src_stat = simple_stat(src);
	Simple_Stat dest_stat = simple_stat(dest);

	if( stat_error( src_stat ) ) {
		fprintf( stderr, "ERROR_016 : %s\n", stat_error_string( src_stat ) );
		return -1;
	}

	if( stat_error( dest_stat ) ) {
		fprintf( stderr, "ERROR_017 : %s\n", stat_error_string( dest_stat ) );
		return -1;
	}

	if( !stat_not_exist( dest_stat ) ) {
		if( stat_symlink( dest_stat ) ) {
			if (remove( dest ) != 0) {
				fprintf( stderr, "ERROR_018 : Unable to delete %s\n", dest ); 
			}
		} else {
			fprintf( stderr, "ERROR_019 : \"%s\" is not a symlink as it should be\n", dest ); 
		}
	}

	// note that src is already an absolute path in this program
	if( symlink( src, dest ) != 0 ) {
		fprintf( stderr, "ERROR_020 : %s\n", strerror(errno) );
		return -1;
	}
	if( always_chmod ) {
		chmod(dest,src_stat.mode);
	}
	return 0;
}