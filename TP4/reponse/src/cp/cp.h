#pragma once

#include "../types.h"

int cp_file( const string src, const string dest, bool always_chmod );
int cp_symlink( const string src, const string dest, bool always_chmod );
