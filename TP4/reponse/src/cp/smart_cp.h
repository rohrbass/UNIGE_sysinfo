#pragma once

#include "../types.h"

void smart_cp( string src, string dest, bool symlink_mode, bool stop_on_failure, bool always_chmod );
