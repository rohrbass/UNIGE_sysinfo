#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include "smart_cp.h"
#include "cp.h"
#include "../string.h"
#include "../simpler_stat/simpler_stat.h"

#include <libgen.h>

// src must be an absolute path
// dest must be an absolute path to an existing directory
void smart_cp( const string src, const string dest, bool symlink_mode, bool stop_on_failure, bool always_chmod ) {

	Simple_Stat src_stat = simple_stat( src );
	Simple_Stat dest_stat = simple_stat( dest );

	// first check if everything is fine

	if( stat_error( src_stat ) ) {
		printf( "ERROR_001: \"%s\" skipped : %s\n", src, stat_error_string( src_stat ) );
		if( stop_on_failure ) { exit(EXIT_FAILURE); }
		return;
	}

	if( stat_error( dest_stat ) ) {
		printf( "ERROR_002: \"%s\" skipped : %s\n", dest, stat_error_string( dest_stat ) );
		if( stop_on_failure ) { exit(EXIT_FAILURE); }
		return;
	}

	if( stat_not_exist( src_stat ) ) {
		printf( "ERROR_003: %s does not exist !\n", src );
		if( stop_on_failure ) { exit(EXIT_FAILURE); }
		return;
	};

	// check if dest is a correct directory

	if( stat_not_exist( dest_stat ) ) {
		// let's make a new one ( mkdir 775 )
		// if( mkdir( dest, S_IRWXU | S_IRWXG | S_IXOTH | S_IROTH ) != 0 ) {
		// 	fprintf( stderr, "ERROR : could not mkdir \"%s\"\n", dest );
		// 	if( stop_on_failure ) { exit(EXIT_FAILURE); }
		// 	return;
		// }
		fprintf( stderr, "ERROR_004 : \"%s\" does not exist\n", dest );
		if( stop_on_failure ) { exit(EXIT_FAILURE); }
		return;
	} else {
		if( stat_directory( dest_stat ) ) {

		} else {
			// is not a directory !
			fprintf( stderr, "ERROR_005 : the destination \"%s\" should be a directory but it is not\n", dest );
			if( stop_on_failure ) { exit(EXIT_FAILURE); }
			return;
		}
	}

	// check target
	string tmp = strdup(src);
	string current_file = basename(tmp);
	free(tmp);
	string target = string_concatenate2( dest, "/", current_file );

	Simple_Stat target_stat = simple_stat( target );

	if( stat_symlink( src_stat ) && symlink_mode ) {
		// symlink mode
		cp_symlink( src, target, always_chmod );
		return; // ! TODO check memory leak
	}

	if( stat_file( src_stat ) ) {
		if( !stat_not_exist( target_stat ) && !stat_file( target_stat ) ) {
			fprintf( stderr, "ERROR_006 : \"%s\" already exists as not a file, cannot write file onto it\n", target );
			if( stop_on_failure ) { exit(EXIT_FAILURE); }
			return;
		} else {
			if ( cp_file( src, target, always_chmod ) != 0 ) {
				if( stop_on_failure ) { exit(EXIT_FAILURE); }
			}
		}
	}

	if( stat_directory( src_stat ) ) {
		// let's avoid a skipping mechaisme for directory
		// as there are too many edge cases

		if( !stat_not_exist( target_stat ) && !stat_directory( target_stat ) ) {
			fprintf( stderr, "ERROR_007 : \"%s\" already exists as not a directory, cannot make directory onto it\n", target );
			if( stop_on_failure ) { exit(EXIT_FAILURE); }
			return;
		} else {
			mkdir( target, src_stat.mode );
		}

		DIR *d = opendir( src );
		if (d) {
			struct dirent *dir;
			while ((dir = readdir( d )) != NULL) {
				if( strcmp(dir->d_name, "." ) == 0 || strcmp(dir->d_name, ".." ) == 0 ) {
					// ignore . and ..
					continue;
				}

				string new_src = string_concatenate2( src, "/", dir->d_name );
				// printf("SOURCE %s\nTARGET %s\n\n", new_src,target);
				smart_cp( new_src, target, symlink_mode, stop_on_failure, always_chmod );
				free( new_src );
			}
			closedir(d);
		} else {
			fprintf( stderr, "ERROR_008 : could not open \"%s\"\n", src );
			if( stop_on_failure ) { exit(EXIT_FAILURE); }
			return;
		}
	}
}
