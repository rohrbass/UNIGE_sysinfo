#pragma once
#include <string.h>

#include "types.h"

string new_string( size_t length );
string string_concatenate( string left, string right );
string string_concatenate2( string left, string middle, string right );
// string string_concatenate_array( Array(string) strings, string separator );
