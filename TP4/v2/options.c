#include "options.h"

int getoptions(int argc, char * argv[], bool * remplacer_droits, bool * copier_liens, int * n)
{
	/* set default values */
	* remplacer_droits = false; 
	* copier_liens = false; 
	* n = 0; 

	/* get options */ 
	int opt; 
	while((opt = getopt(argc, argv, "haf")) != -1)
	{
		switch (opt) 
		{
			case 'h': 
				/* help */
				printf("This program operates under two modes : \n\n");

				printf("Usage : %s [-h] name \n", argv[0]); 
				printf("This program displays recursively the contents of file or directory name. \n"); 
				printf(" -h  : prints this message\n\n"); 

				printf("Usage : %s [-h] [-a] [-f] sources ... destination \n", argv[0]); 
				printf("This mode copies recursively the contents of sources into destination. \n"); 
				printf(" -h  : prints this message\n");               
				printf(" -a  : file rights should be modified on copy \n");
				printf(" -f  : links should not be converted to files on copy\n");
				printf("\n"); 
				return 1; 
			
			case 'a': 
				/* should rights be replaced ? */ 
				* remplacer_droits = true; 
				break; 

			case 'f': 
				/* should links not be converted to files*/ 
				* copier_liens = true; 
				break; 

			default: 
				/* other flags */ 
					fprintf(stderr, "Usage : %s [-h] name \n", argv[0]);
					fprintf(stderr, "Usage : %s [-h] [-a] [-f] sources ... destination \n", argv[0]); 
				return -1; 
		}
	}

	/* test if arguments have been passed to the function */ 
	if(optind>=argc)
	{
		fprintf(stderr, "Expected argument after options \n"); 
		return -2; 
	}

	*n = optind; /* number of parameters passed that need to be processed by our program */ 

	return 0; 
}
