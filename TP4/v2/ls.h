#include <stdio.h> 
#include <sys/types.h> 
#include <sys/stat.h> 
#include <time.h> 
#include <dirent.h>
#include <string.h> 
#include <limits.h> 

#ifndef LSH
#define LSH

int print_info(const char * path); 
int my_ls(const char * path); 

#endif 
