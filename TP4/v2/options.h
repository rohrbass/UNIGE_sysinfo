#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 

/* défninir un type booléen */ 
#ifndef BOOL 
#define BOOL 
typedef int bool; 
enum {false, true};
#endif

#ifndef OPTIONSH
#define OPTIONSH

/* GETOPTIONS
*** WHAT IT DOES : 
gets the options provided the user and processes it, so the program only needs to handle arguments 

*** IN : 
argc 				count of the arguments passed to the main function of hash.c 
argv				arguments passed to the main function of hash.c 
remplacer_droits 	0 if we want to replace rights, 1 otherwise
copier_liens 		0 if we want to copy links as files, 1 otherwise
n					(technical) where we stopped reading arguments, so the module 2 can
				parse directory / filename inputs.

*** OUT : error message : 
1				Function just displayed help
0 				Terminated correctly
*/

int getoptions(int argc, char * argv[], bool * remplacer_droits , bool * copier_liens, int * n); 


#endif
