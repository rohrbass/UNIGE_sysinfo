#include "ls.h" 

/* RELATED MAN PAGES : 
stat (2)
inode (7)
*/ 

int print_info(const char * path)
{
	/* define structure to take in the result of stat */ 
	struct stat sb; 

	/* get stat info, check if that went well */ 
	if(stat(path, &sb)==-1)
	{
		perror("Stat internal error\n"); 
		return -1; 
	}

	/* get the file type */ 
	char type_of_file; 
	switch(sb.st_mode & S_IFMT)
    {
		/* block device */ 
        case S_IFBLK:   type_of_file = 'b'; 	            break;
		/* character device */ 
        case S_IFCHR:   type_of_file = 'c'; 		        break;
		/* directory */ 
        case S_IFDIR:   type_of_file = 'd'; 	            break;
		/* FIFO/pipe */ 
        case S_IFIFO:   type_of_file = 'p'; 	            break;
		/* symlink */ 
        case S_IFLNK:   type_of_file = 'l';	 	            break;
		/* regular file */ 
        case S_IFREG:   type_of_file = '-';			        break;
		/* socket */ 
        case S_IFSOCK:  type_of_file = 's';					break;
		/* otherwise */ 
        default:        type_of_file = '?';                 break;
    }


	/* get the rights */ 
	char rights[10]; 
	/* user permissions (read-write-execute) */ 
	rights[0] = (sb.st_mode & S_IRUSR) ? 'r' : '-'; 
	rights[1] = (sb.st_mode & S_IWUSR) ? 'x' : '-'; 
	rights[2] = (sb.st_mode & S_IXUSR) ? 'w' : '-'; 
	/* group permissions (read-write-execute) */ 
	rights[3] = (sb.st_mode & S_IRGRP) ? 'r' : '-'; 
	rights[4] = (sb.st_mode & S_IWGRP) ? 'x' : '-'; 
	rights[5] = (sb.st_mode & S_IXGRP) ? 'w' : '-'; 
	/* other permissions (read-write-execute) */ 
	rights[6] = (sb.st_mode & S_IROTH) ? 'r' : '-'; 
	rights[7] = (sb.st_mode & S_IWOTH) ? 'x' : '-';
	rights[8] = (sb.st_mode & S_IXOTH) ? 'w' : '-'; 
	/* end of string */ 
	rights[9] = '\0'; 
	

	/* retrieve time info */ 
	char date[40];
	/* convert to tm structure */ 
	struct tm * time = localtime(&sb.st_mtime); 
	/* format along the wanted format */ 
	strftime(date, sizeof(date), "%a %b %e %T %Y", time); 
	/* display info */ 
	printf("%c%-10s %-10lld %s   %s\n", type_of_file, rights, (long long) sb.st_size, date, path);

	return 0; 
}

int my_ls(const char * path) 
{
	/* open the directory passed as argument */ 
	DIR * dir = opendir(path); 
	if(dir==NULL)
	{
		fprintf(stderr, "Cannot open directory %s\n", path); 
		return -1; 
	}

	/* read the elements of the directory */ 
	struct dirent *entry; 
	while((entry=readdir(dir)) != NULL) 
	{
		/* ignore files ".." and "." */ 
		if(strcmp(entry->d_name, "..") && strcmp(entry->d_name, "."))
		{
			/* initialise variable for recursive search */ 
			char new_dir[PATH_MAX]; 

			/* check which path format the user has specified */
			if(*(path + strlen(path) -1)=='/')
			{
				/* director name for recursive search */ 
				snprintf(new_dir, PATH_MAX, "%s%s", path, entry->d_name); 
				/* display the file info */
				print_info(new_dir); 

			}
			else
			{
				/* director name for recursive search */ 
				snprintf(new_dir, PATH_MAX, "%s/%s", path, entry->d_name); 
				/* display the file info */
				print_info(new_dir); 
			}

			/* check if the considered file is a directory */ 
			struct stat sb;
			stat(new_dir, &sb); 
			if((sb.st_mode & S_IFMT) == S_IFDIR)
			{
				my_ls(new_dir); 
			}
		}
	}

	return 0; 	
}
