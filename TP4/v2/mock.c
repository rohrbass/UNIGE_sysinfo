#include "cp.h" 

int main(int argc, char ** argv)
{
	if(argc!=3)
	{
		fprintf(stderr, "please give appropriate number of args (2)\n"); 
		return -1; 
	}
	copy_to_file(argv[1], argv[2], false); 
	return 0; 
}
