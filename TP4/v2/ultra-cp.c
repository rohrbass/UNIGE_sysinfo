#include <stdio.h> 
#include "options.h" /* pour gérer les options */ 
#include "ls.h"
#include "cp.h"

int main(int argc, char * argv[]) 
{
	/* vérifier si au moins un paramètre a été passé en option */ 
	if(argc==1)
	{
		fprintf(stderr, "%s requires at least an argument. \nFor more information on usage : %s -h\n", argv[0], argv[0]); 
		return -1; 
	}
	
	
	/* gérer les options */ 
	int n ; 
	bool remplacer_droits;  
	bool copier_liens; 

	int err1 = getoptions(argc, argv, &remplacer_droits, &copier_liens, &n); 
	
	/* gestin des erreurs */ 
	switch (err1)
	{
		case 0: 
			break; 

		case 1: 
			/* displayed help */ 
			return 0; 

		case -1:
			/* misusage of options */
			return -1; 

		case -2: 
			/* argument should be passed to the function */ 	
			return -1; 
	}

	printf("remplacer_droits = %d\n", remplacer_droits); 
	printf("copier_liens = %d\n", copier_liens); 
	printf("n = %d\n", n); 




	int exit_code; 

	/* accéder les deux différent modes */ 
	if(argc==2)
	{
		/* NOTE: pas besoin de vérifier si des flags ont étés passés, 
		   si just flag --> erreur lors de la gestion des options */ 

		/* enter display mode */ 
		exit_code = my_ls(argv[1]); 
	}
	else 
	{
		/* enter copy mode */ 
		exit_code = my_cp(argc, argv, n, remplacer_droits, copier_liens); 

	}

	return exit_code; 
}
