#include "cp.h" 

int check_file_type(char * nom)
{
	/* we use open for checking existence */ 
	int file_desc = open(nom, O_CREAT|O_EXCL, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH); 

    if(file_desc<0)
    {  
        if(errno==EEXIST)
        {  
            struct stat sb; 
            if(stat(nom, &sb) == -1)
            {  
                perror("Stat internal error"); 
                return -1; 
            }  

            switch(sb.st_mode & S_IFMT)
            {  
                case S_IFDIR: 
                    /* this is a directory */ 
                    return REPERTOIRE; 
                case S_IFREG: 
                    /* this is a file */ 
                    return FICHIER; 
                default: 
                    printf("Last argument type fail : You need to specify a file or a directory\n"); 
                    return AUTRE;
            }          
        }      
           
        else
        {  
            perror("open internal error");
			printf("hint : provide a directory name in the format hello and not hello/\n"); 
            return -1; 
        }      
    }      
    else
    {  
		/* close the file */ 
        int err = close(file_desc);
        switch (err) 
        {  
            case 0:
                /* close worked well */
                break;   
            case -1: 
                /* close failed */ 
				perror("Close internal failiure"); 
                return -1; 
        }

        printf("file created : %s\n", nom);
        return FICHIER; 
    }      
}




int go_through_dir(char * src_path, char * dest_path)
{
	/* open directory */ 
	struct dirent * entry; 
	DIR * dir = opendir(src_path); 
	if(dir==NULL)
	{
		fprintf(stderr, "Cannot open directory %s\n", src_path); 
		return -1; 
	}

	/* read directory files */ 
	while((entry=readdir(dir)) != NULL)
	{
		/* check if the file isn't . or .. */ 
		if(strcmp(entry->d_name, "..") && strcmp(entry->d_name, "."))
		{
			/* define new path names */
			char new_src_dir[PATH_MAX]; 
			char new_dest_dir[PATH_MAX]; 
			int new_dir_len, new_dest_len; 
			if(*(src_path +strlen(src_path) - 1) =='/')
			{
				new_dir_len = snprintf(new_src_dir, PATH_MAX, "%s%s", src_path, entry->d_name); 
				new_dest_len = snprintf(new_dest_dir, PATH_MAX, "%s%s", dest_path, entry->d_name); 
			}
			else
			{
				new_dir_len = snprintf(new_src_dir, PATH_MAX, "%s/%s", src_path, entry->d_name); 
				new_dest_len = snprintf(new_dest_dir, PATH_MAX, "%s/%s", dest_path, entry->d_name); 
			}
			if((new_dir_len >= PATH_MAX) || (new_dest_len >= PATH_MAX))
			{
				fprintf(stderr, "Path name is too long\n"); 
				return -2; 
			}

			/* define stat structure */ 
			struct stat sb; 
			if(stat(new_src_dir, &sb) == -1)
			{
				fprintf(stderr, "Stat internal error \n"); 
				return -1; 
			}
			/* actually check for file type : regular file, directory, symbolic link */ 
			if(S_ISREG(sb.st_mode) || S_ISDIR(sb.st_mode) || S_ISLNK(sb.st_mode))
			{
				/* call the copy to file */ 
				printf("make a copy \n"); 
				/* TODO : implement this, requires: gestion des parametres (-a), check that rights are preserved */ 
			}

			/* recursive call */ 
			if((sb.st_mode & S_IFMT)==S_IFDIR)
			{
				go_through_dir(new_src_dir, new_dest_dir); 
			}

			
		}
	}
	return 0; 
}
/* TODO: go over the whole code with option managment */ 



int copy_to_dir(int argc, char **argv, int n)
{
	for(int i=0; i<(argc -2); i++)
	{
		int err = go_through_dir(argv[i+n], argv[argc -1]); 
		/* deal with errors */ 
		if(! err)
		{
			/* something has gone wrong */ 
			return -1; 
		}
	}
	return 0; 
}




int copy_to_file(char * src_file, char * dest_file, bool remplacer_droits)
{
		/* open the source file */ 
		int src_fd = open(src_file, O_RDONLY); 
		if(src_fd<0)
		{
			perror("Open internal error on opening src file"); 
			return -1; 
		}
		
		/* open the destination file */ 
		int dest_fd = open(dest_file, O_WRONLY); 
		if(dest_fd<0)
		{
			perror("Open internal error on opening dest file"); 
			return -1; 
		}

		/* retrieve rights, size and modification date */ 
		struct stat sb_src;
		struct stat sb_dest; 
		if((stat(src_file, &sb_src) ==-1) || (stat(dest_file, &sb_dest)==-1))
		{
			fprintf(stderr, "Stat internal error \n"); 
			return -1; 
		}

		/* check if size and modification dates are different */ 
		bool copier; 
		copier = ((sb_src.st_size!=sb_dest.st_size)||(sb_src.st_mtime!=sb_dest.st_mtime))?true:false; 

		if(copier)
		{
			/* read the source file */ 
			char buff[4069]; 
			ssize_t len_read; 
			while((len_read = read(src_fd, buff, sizeof(buff)))>0)
			{
				/* write to the destination file */ 
				write(dest_fd, buff, len_read); 
			}

			/* close files */ 
			int err_src = close(src_fd); 
			switch (err_src)
			{
				case 0: 
					break; 
				case -1: 
					perror("Close internal error"); 
					return -1; 
			}
			int err_dest = close(dest_fd); 
			switch (err_dest)
			{
				case 0: 
					break; 
				case -1: 
					perror("Close internal error"); 
					return -1; 
			}
		
			printf("Succesfully copied %s to %s\n", src_file, dest_file); 
			return 0; 
		}	
		else
		{
			printf("not copying file.\n"); 
			if(remplacer_droits)
			{
				/* TODO : do a chmod */ 
			}
		}
		return 0; 
}




int my_cp(int argc, char ** argv, int n, bool remplacer_droits, bool copier_liens)
{
	/* determine which if the last argument is a file or a directory, 
	if the file doesn't exist, create a regular file with that name */
	int type = check_file_type(argv[argc-1]); 

	/* act in consequence to the type */ 
	switch(type)
	{
		case REPERTOIRE: 
		{
			/* it is a directory */ 
			printf("it is a directory\n"); 

			int err = copy_to_dir(argc, argv, n); 

			/* deal with errors */ 
			switch(err)
			{
				case 0: 
					break;  
			}
			break; 
		}
		
		case FICHIER:
		{
			/* it is a file */ 

			/* check if exactly two arguments have been passed by the user */ 
			if(argc==3)
			{
				/* check that source file is a regular file */
				struct stat sb; 
				if(stat(argv[1], &sb) == -1)
				{
					perror("Stat internal error"); 
					return -1; 
				}
				if((sb.st_mode & S_IFMT)!=S_IFREG)
				{
					fprintf(stderr, "Error : when copying to a regular file, source should be a regular file too\n"); 
					return -1; 
				}
			}
			else
			{
				printf("Error : for copying to a file, only one source file accepted\n"); 
				return -2; 
			}

			/* call copy function */ 
			int err = copy_to_file(argv[argc-2], argv[argc-1], remplacer_droits); 
			
			/* deal with errors */ 
			if(err) 
			{
				return -1; 
			}
			break; 
		}

		case -1: 
			/* error with open() and close() */ 
			return -1; 
		
		case AUTRE: 
			/* error with argument type passed */ 
			return -1; 
	}
return 0; 
}
