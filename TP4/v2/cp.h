#include <dirent.h> 
#include <errno.h> 
#include <fcntl.h>
#include <limits.h> 
#include <stdio.h> 
#include <string.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h>
#include "options.h" 

#ifndef CPH
#define CPH

#ifndef BOOL 
#define BOOL 
typedef int file_type; 
#endif

enum{REPERTOIRE, FICHIER, LIEN, AUTRE};

int check_file_type(char * nom); 

int copy_to_dir(int argc, char ** argv, int n); 

int copy_to_file(char * src_file, char * dest_file, bool remplacer_droits); 

int my_cp(int argc, char ** argv, int n, bool remplacer_droits, bool copier_liens); 

#endif
