#SHLL

`SHLL` stands for `SHitty sheLL`™. It's not practical, it has almost no features.

# issues

- if we begin typing and then delete, sometimes deletes the command prompt 

# TODO

- use `sigaction` rather than `signal`
- test s'il y a des zombies 
- relire le code et faire en sorte qu'il marche en multithread 
- bien gérer les signaux, et race-conditions
- il doit y avoir deux bool de définis, enlever celui qui n'est pas dans utils/utils.h
- faire un tableau global pour garder les pid, pour pouvoir utiliser waitpid dans le handler 
- die on \0
- catch sigterm (so that we can `ctrl+c` like a boss)
- tester sur un program qui se fait interrompre (process -- exited.)
- le script run_program/run.* est (presque) obsolète (contient encore le handler) 
- arrow key -> history
- autocomplete ? this may be hardcore
- cd with 0 args -> cd ~

# notes

We don't free resources before `exit` because the OS will to that for us :¬D
