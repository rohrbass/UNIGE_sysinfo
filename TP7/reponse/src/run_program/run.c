#include "run.h" 

#include <errno.h> 
#include <linux/limits.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/wait.h> 
#include <unistd.h> // pour la variable environ

#include "global.h" 


/* ------------------- DAD ----------------------------- */ 
/* used to help dad burry his kid */ 
void handler_func(int signum)
{// attention -> remplacer tous les printf par write (handler = que fnct réentrantes)
	if(signum==SIGCHLD)
	{
		int status; 
		int err = waitpid(proc_id, &status, WNOHANG); 
		if(err==-1)
		{
			perror("wait internal error");
		}

		if(err==0)
		{
			printf("Process %d exited.\n", proc_id); 
		}
		else
		{
			printf("Process %d exited with status : %d\n", proc_id, status);
		}
	}
}

/* -------------------- KID ----------------------------- */ 



/* used to help the kid launch the program he needs to lauch */ 
/*
void run_the_program(char ** argv, char ** envp)
{
	// TODO : make that code more pretty :) 

	// get PATH variable 
	char * PATH = getenv("PATH"); 
	char * my_path = strdup(PATH); 

	// take a path in the variable PATH 
    char * where_is; 
    int iter = 0; 
    while(1)
    {  
        // take a path in the variable PATH 
        if(iter) // != 0 
        {  
            where_is = strtok(NULL, ":"); 
            if(!where_is) // where_is == NULL
            {  
                printf("function %s unknown\n", argv[0]); 
                exit(EXIT_FAILURE); 
            }  
        }  
		else
        {  
            where_is = strtok(my_path, ":");
            if(!where_is)
            {
                printf("bad PATH variable\n");
				exit(EXIT_FAILURE); 
            }
        }
        iter += 1;

        // create path to executable
        char path_to_file[PATH_MAX];
        memset(path_to_file, 0, PATH_MAX);
        strcat(path_to_file, where_is); // TODO : check buffer overflow
        strcat(path_to_file, "/");
        strcat(path_to_file, argv[1]);

        // execute the file - if does not exist, try another path
        execve(path_to_file, argv, envp);
		if(errno==ENOENT) // fichier pas trouvé
		{
			continue;
		}
		else
		{
			perror("run: execve internal error");
			exit(EXIT_FAILURE);  
		}
	}

	// free up ressources
	free(my_path); 

	exit(EXIT_SUCCESS); 
}
*/
