#pragma once

#include "any_arrays.h"

// bool
typedef enum {false, true} bool; 

// strings
typedef char* string;
string new_string(size_t length);
void free_string(string str);

// dynamic arrays
prototype_array_of(string);
