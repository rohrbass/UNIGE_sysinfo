#include "types.h"

implement_array_of(string);

string new_string(size_t max_length) {
	string s = malloc(max_length * sizeof(char));
	s[0]='\0';
	return s;
}

void free_string(string str) {
	free(str);
}
