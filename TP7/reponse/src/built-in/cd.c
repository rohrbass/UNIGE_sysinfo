#include "built-in-commands.h" 

#include <string.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <linux/limits.h> 

int cd( int argc, char *argv[] ) {

	/* check if right number of arguments have been passed */ 
	if(argc !=2) {
		printf("cd : Wrong number of arguments passed\n"); 
		return -1; 
	}

	/* create a variable for old path */ 
	char * current_path = malloc(PATH_MAX*sizeof(char)); 

	/* get current path */ 
	char * ptr = getcwd(current_path, PATH_MAX); 
	if(ptr==NULL) {
		perror("cd : "); 
		return -1; 
	}

 	char * where_to = argv[1]; 

	/* if the dest. path ends with '/', remove it */
	if(where_to[strlen(where_to) -1] == '/') {
		where_to[strlen(where_to) -1] = '\0'; 
		argv[1] = where_to; 
		return cd(argc, argv); 
	}

	/* create the string of the new path */ 
	strcat(current_path, "/"); 
	strcat(current_path, where_to); 

	/* actually change directory */ 
	int err = chdir(current_path); 
	if(err!=0) {
		perror("cd :");
		return -1; 
	}

	/* free ressources */ 
	free(current_path); 

	return 1; 
}
