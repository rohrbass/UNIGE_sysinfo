#pragma once

#include "utils/types.h"

/* get_builtin 
 *  OUT : 
 *    - a function pointer to a corresponding command
 *    - NULL if command not found
 *  IN  :
 *    - string command (requested command)
 */ 
int ( *shll_get_builtin( string command ) ) (int argc, char ** argv);
