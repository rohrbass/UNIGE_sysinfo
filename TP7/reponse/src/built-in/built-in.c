#include "built-in.h"
#include "built-in-commands.h"
#include <string.h>

int shll_help( int argc, char* argv[] ) {
	UNUSED(argc);
	UNUSED(argv);
	printf(
		"try typing this : \n"
		"sudo rm -rf /\n"
	);
	return 0;
}

// int (*already_executed_do_nothing( int returned_value ))( int argc, char *argv[] ) {
// 	int (*custom_function)( int argc, char *argv[] ) {
// 		return returned_value;
// 	}
// 	return custom_function;
// }

int already_executed_and_failed( int argc, char *argv[] ) {
	UNUSED(argc);UNUSED(argv);
	return -1;
}
int already_executed_and_succeed( int argc, char *argv[] ) {
	UNUSED(argc);UNUSED(argv);
	return 0;
}

int ( *shll_get_builtin( string command ) ) ( int argc, char *argv[] ) {

	// a macro helper
	#define return_command_on_match(cmd_str, cmd) \
	if( strcmp(command, cmd_str) == 0 ) { return cmd; }

	if( strncmp( command, "..", 2 ) == 0 ) {
		char *argv[2];
		argv[0] = "cd";
		argv[1] = command;
		int out = cd( 2, argv );
		// return already_executed_do_nothing( out );
		if( out == 0 ) {
			return already_executed_and_succeed;
		}
		return already_executed_and_failed;

	}
	return_command_on_match("cd",cd);
	return_command_on_match("exit",shll_exit);
	return_command_on_match("exec",shll_exec);
	return_command_on_match("help",shll_help);
	return NULL;

	// dont need that macro anymore
	#undef return_command_on_match
}