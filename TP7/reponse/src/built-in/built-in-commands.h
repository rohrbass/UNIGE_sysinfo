#pragma once

/* CD --------------------------------------
 * build-in command for changing directory
 */
int cd( int argc, char *argv[] ); 

/* SHLL_EXIT -------------------------------
 *  build-in command that exits the shell and 
 *  kills it's children 
 */
int shll_exit( int argc, char* argv[] );

/* SHLL_EXEC --------------------------------
 * executes a command without forking 
 */ 
int shll_exec( int argc, char* argv[] );
