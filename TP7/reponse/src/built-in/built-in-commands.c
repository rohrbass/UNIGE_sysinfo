#include "built-in-commands.h"
#include "utils/utils.h"
#include "stdlib.h"
#include "stdio.h"

#include <unistd.h>

int shll_exit( int argc, char* argv[] ) {
	UNUSED(argc);
	UNUSED(argv);
	// TODO : tuer les enfants puis se suicider soi-même
	exit(EXIT_SUCCESS);
}

int shll_exec( int argc, char* argv[] ) {
	UNUSED(argc);
	// ignore "exec"
	if( execvp( argv[1], argv + 1 ) == -1 ) {
		perror("exec failed"); 
		exit(EXIT_FAILURE); 
	}
	return 0;
}
