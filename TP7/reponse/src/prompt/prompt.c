#include "prompt/prompt.h"

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <linux/limits.h> /* PATH_MAX definition */ 

#include "utils/utils.h"

int shll_print_prompt()
{
	/* get current path */ 
	char path[PATH_MAX];
	char * ptr = getcwd(path, PATH_MAX); 
	if(!ptr)
	{
		perror("getcwd internal error"); 
		return -1; 
	}



	/* get username */ 
	char * user = getenv("USER"); // DO NOT FREE ! 
	if(!user)
	{
		printf("could not get user name"); 
		return -1; 
	}

	#define HOST_NAME_MAX 512
	#define LOGIN_NAME_MAX 512

	char hostname[HOST_NAME_MAX];
	char username[LOGIN_NAME_MAX];
	gethostname(hostname, HOST_NAME_MAX);
	getlogin_r(username, LOGIN_NAME_MAX);

	/* TODO : get hostname */ 
	// char * hostname = "hostname"; 

	#undef HOST_NAME_MAX
	#undef LOGIN_NAME_MAX

	/* display prompt */ 
	term_color_green(); 
	printf("%s@%s", user, hostname); 
	term_color_normal(); 
	printf(":"); 
	term_color_blue(); 
	printf("%s", path); 
	term_color_normal();
	printf("$ ");

	return 0; 
}
