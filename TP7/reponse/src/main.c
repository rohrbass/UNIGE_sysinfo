#include "args/args.h" /* options for our bash program */ 
#include "prompt/prompt.h" /* display a prompt */
#include "user_args/user_args.h" /* get user inputed arguments */ 
#include "built-in/built-in.h" /* change directory command */ 
#include "run_program/run.h" /* dad handler funciton and kid run_the_program function */ 
#include "utils/utils.h" /* booleans */ 
#include "global.h" /* global variables */ 

#include <stdio.h> 
#include <sys/types.h> /* les 3 suivants sont pour open */  
#include <sys/stat.h> 
#include <fcntl.h> 
#include <unistd.h> 
#include <signal.h> 

/* values internal to the hash function */ 
/* when changing those values, check that the hash function does not produce collisions */ 
/* #define HASH_P = 151
#define HASH_N = 53
*/ 

int main( int argc, char **argv ) {
	struct sigaction action;

	// action.sa_handler = 
	// action.sa_sigaction = 
	// action.sa_mask = 
	// action.sa_flags = 

	sigaction( SIGTERM, &action, NULL );
	sigaction( SIGQUIT, &action, NULL );

	/* get options */ 
	WTD wtd = shll_parse_args( argc, argv ); // cf. args/ 

	// TODO: if scripts to run in WTD, then run them
	UNUSED( wtd );

	while(1) {
		// TODO : blocker - tout executer en un coup - utiliser un autre handler ?  

		/* TODO : make it more fancy, or not */
		shll_print_prompt(); // cf. prompt/

		/* retrieve user input in the format argc, argv */ 
		user_input * input = shll_read_input(); // cf. user_opt/ 

		if( input == NULL ) {
			fprintf( stderr, "FATAL ERROR : input could not be read\n" );
			continue;
		}

		// if nothing to be done...
		if( input->argc == 0 ) {
			continue;
		}

		// TODO : deblocker, ici ou autrepart ? 

		/* get the command that the user wants to run */ 

		int (*command)(int,char**) = shll_get_builtin( input->argv[0] );

		// bool can_be_executed = shll_can_be_executed( input->argv[0] );
		bool can_be_executed = true;


		if( command ) {
			command( input->argc, input->argv );
		} else if( can_be_executed ) {
			// fork the process 
			proc_id = fork(); 
			if(proc_id == -1) {
				perror("main : fork fail :"); 
				break; 
			}

			// child 
			if(proc_id==0) {
				if(input->background == true) {
					// redirect output 
					int fd_devNull = open("", 0, O_WRONLY); 
					int err = dup2(fd_devNull, 1);
				}

				//run_the_program(input->argv, envp);
				int err = execvp( input->argv[0], input->argv ); 
				if(err==-1) {
					perror("child >> main: execvp:"); 
					exit(EXIT_FAILURE); 
				}
			} else {
			// parent 
				printf("Process %d created.\n", getpid());

				// initialise the sigaction structure 
				struct sigaction sa; 
				sa.sa_handler = &(handler_func);
				sa.sa_flags = 0; 
				sigfillset(&sa.sa_mask); // masquer tous les signaux recus pendant l'execution du handler

				sigaction(SIGCHLD, &sa, NULL); 

				if(input->background==false)
				{
					pause();
				}
			}

		} else {
			printf("'%s' command not found\n", input->argv[0]);
		}

		// free ressources
		free_user_input(input);
	}
	return 0; // TODO : useless ??
}
