#pragma once

#include "../utils/types.h" //include boolean type

/* structure to return user arguments */ 
typedef struct {
	int argc; 
	char ** argv; 
	bool background; 
} user_input;

/* FREE_USER_INPUT --------------------------------
function to free a pointer to the structure user_input 

IN : 
the pointer to the structure

OUT : 
None
*/ 
void free_user_input( user_input * my_struct ); 



/* SHLL_READ_INPUT --------------------------------
TODO : Exp 

IN : 
None  

OUT : 
user_input structure (see above) or NULL pointer in case of an error
/!\ BEWARE memory needs to be freed after using the structure, 
	to do so, please use free_user_input() (cf. above) /!\
*/
user_input * shll_read_input();
