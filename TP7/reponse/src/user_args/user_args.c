#include "user_args.h" 

#include <stdlib.h> 
#include <stdio.h> 
#include <string.h> 

/* maximum size of the buffer that can contain options passed by user */ 
#define ARGV_MAX_SIZE 32768

string read_line() {
	char * buff = malloc( ARGV_MAX_SIZE * sizeof(char) );
	char * perr = fgets( buff, ARGV_MAX_SIZE, stdin );
	if(!perr) {
		free(buff);
		return NULL;
	}
	perr[strlen(buff) - 1] = '\0'; // replace return character by a \0 char
	return buff;
}

user_input *parse_one_command( string one_command, bool background ) {
	/* initialise the return structure */
	user_input *input = malloc( sizeof(user_input) );
	input->argc = 0;
	input->argv = NULL;
	input->background = background;

	/* parse arguments */
	char * result;
	result = strtok(one_command, " \t\r\n\v\f"); // any separator


	if(result != NULL) {
		input->argc += 1;
		input->argv = realloc(input->argv, (input->argc)*sizeof(char *));
		(input->argv)[(input->argc)-1] = result;
	}

	while(result != NULL) {
		result = strtok(NULL, " \t\r\n\v\f"); // any separator
		if(result != NULL) {
			input->argc += 1;
			input->argv = realloc(input->argv, (input->argc)*sizeof(char *));
			(input->argv)[(input->argc)-1] = result;
		}
	}

	/* finish the argv */ 
	input->argv = realloc(input->argv, (input->argc + 1)*sizeof(char *));
	input->argv[input->argc] = NULL;

	return input;
}

user_input *shll_read_input() {
	/* get input from user */ 
	string line = read_line();

	// TODO : split line by & and |
	// step 1 : split  by & and |
	// step 2 : filter by & and | -> identify

	// handle ctrl+D
	if( line == NULL ) {
		user_input *input = malloc( sizeof(user_input) );
		input->argc = 1;
		input->argv = malloc( 2 * sizeof(char));
		input->argv[0] = "exit";
		input->argv[1] = NULL;
		return input;
	}

	bool background = false;
	user_input *input = parse_one_command( line, background );


	// /* check if we should run the process in the backround */
	// if(!strcmp(input->argv[(input->argc)-1], "&"))
	// {
	// 	input->background = true;
	// 	input->argc -=1;
	// 	input->argv = realloc(input->argv, (input->argc)*sizeof(char *));
	// }
	

	/* return the result */ 
	return input;
}

void free_user_input(user_input * my_struct)
{
	free((my_struct->argv)[0]);
	free(my_struct->argv);
	free(my_struct);
}
