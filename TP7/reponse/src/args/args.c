#include "args.h"
#include <stdlib.h>
#include <stdio.h> 
#include <getopt.h>

void print_help( FILE* fd, char * binary_name )
{
	fprintf( fd,
	"usage : %s [-h] [-c path]\n"
	" -h                     : prints this message\n"
	" -c path/to/config/file : load a config file for this shell\n"
	" -x path/to/script/file : load a file and run it\n"
	, binary_name);
}

WTD new_WTD() {
	WTD wtd = malloc(sizeof(WTD_TYPE));
	wtd->path_to_config_file = NULL;
	wtd->path_to_script_file = new_array(string);
	return wtd;
}

void free_WTD( WTD wtd ) {
	wtd->path_to_config_file = NULL;
	array_free(wtd->path_to_script_file);
}

WTD shll_parse_args( int argc, char ** argv )
{
	WTD wtd = new_WTD();

	int opt; 
	while( (opt = getopt(argc, argv, "-hc:")) != -1 )
	{
		switch( opt )
		{
			case 'h':
				print_help( stdout, argv[0] ); 
				exit(EXIT_SUCCESS);
			case 'c':
				wtd->path_to_config_file = optarg;
				break;
			case 'x':
				array_push( wtd->path_to_script_file, optarg );
				break;
			default: 
				fprintf( stderr, "what were you thinking ? %s ? what ?\n\n",optarg);
				print_help( stderr, argv[0] ); 
				exit(EXIT_FAILURE);
		}
	}
	return wtd; 
}
