#pragma once

#include "utils/types.h"

// what needs to be done ?
typedef struct {
	string        path_to_config_file;
	Array(string) path_to_script_file;
} WTD_TYPE;

// this is a helper to manipulate only pointers
typedef WTD_TYPE* WTD;

// WTD memory management
WTD  new_WTD();
void free_WTD(WTD);

// argument parsing for shll
WTD shll_parse_args(int argc, char ** argv); 
