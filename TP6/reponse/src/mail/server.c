#include <string.h>
#include "server.h"
#include "socket/socket.h"
#include "time/mytime.h"

int connect_server( string host, unsigned int port ) {
	printf("connecting to %s:%u ... ", host, port); fflush(stdout);

	/* try opening a socket and handle errors */ 
	int mail_server_socket = socket_open( host, port );
	if( mail_server_socket < 0 ) {
		printf("failed\n"); fflush(stdout);
	} else {
		printf("success\n");
	}
	return mail_server_socket;
}

int disconnect_server( int socket ) {
	return socket_close( socket );
}

int connect_server_wtd( WTD wtd ) {
	int socket;
	if( wtd.action & ACTION_CUSTOM_PORT ) {
		socket = connect_server( wtd.host, wtd.port );
		if( socket < 0 ) { exit( EXIT_FAILURE ); }
	} else {
		// try default ports
		if( 1
			&& ((socket = connect_server( wtd.host, 587 )) < 0) 
			&& ((socket = connect_server( wtd.host, 25  )) < 0)
			&& ((socket = connect_server( wtd.host, 465 )) < 0)
		) {
			die("ERROR : could not open any default smtp ports\n");
		}
	}
	return socket;
}

int server_is_fine( int code ) {
	/* good status codes specified by smtp documentation */ 
	if( 200 <= code && code <=355 ) {
		return 1;
	}
	return 0;
}

int server_says( int socket ) {
	string s = socket_read( socket );

	/* get message code */ 
	char number[4];
	strncpy(number, s, 3);
	number[3]='\0';
	int status = atoi( number );

	/* check if it is an error code or not, 
	set the terminal color accordingly */ 
	int happy = server_is_fine(status); 
	if( !happy ) {
		term_color_red();
	} else {
		term_color_green();
	}

	/* display message from server */ 
	printf( "%s", s );
	term_color_normal();

	return happy?0:-1;
}


long long int extract_length( string str ) {
	char * pch;
	pch = strtok (str, "\n");
	while ( pch != NULL ) {
		#define nnn "250-SIZE "
		#define nnnn strlen(nnn)
		int is_equal = !strncmp( pch, nnn, nnnn);
		if( is_equal ) {
			return atoll(pch + nnnn);
		}
		#undef nnn
		#undef nnnn
		pch = strtok (NULL, "\n");
	}
	return 0;
}

// returns -1 on failure
// returns 0 if max_size unknown
// returns max_size if known
long long int server_says_ehlo( int socket ) {
	string s = socket_read( socket );

	/* get server's status number */ 
	char number[4];
	strncpy(number, s, 3);
	number[3]='\0';
	int status = atoi( number );

	/* process it */ 
	if( !server_is_fine( status ) ) {
		term_color_red();
	} else {
		term_color_green();
	}

	/* print server status message */ 
	printf( "%s", s );
	term_color_normal();

	long long int max_length = extract_length( s );

	return server_is_fine(status)?max_length:-1;
}


int client_says( int socket, string message ) {
	term_color_cyan();
	printf( "%s",message );
	term_color_normal();
	return socket_write_string( socket, message );
}

int client_says_file( int socket, string path ) {
	term_color_cyan();
	int out = socket_write_file( socket, path );
	term_color_normal();
	return out;
}



/* SERVER_SAYS_EHLO, SERVER_SAYS, CLIENT_SAYS, CLIENT_SAYS_FILE are defined in server.h */ 

int send_email(
	int mail_server_socket,
	string from_mail,
	Array(string) to,
	Array(string) to_cc,
	string subject,
	string path_to_mail_data
) {
	
	// connection is ok ?
	SERVER_SAYS( mail_server_socket );

	// say hello !
	CLIENT_SAYS( mail_server_socket, "EHLO error\n" );
	// SERVER_SAYS( mail_server_socket );
	SERVER_SAYS_EHLO( mail_server_socket );
	//  === TODO ===
	// long long int max_size = SERVER_SAYS_EHLO( mail_server_socket );
	// use max_size on each clusters of CLIENT_SAYS to see if we aren't sending too much stuff
	//  => client_says should evaluate to the amount of data sent, we can accumulate this.
	// throw an error when that amount is overflown.

	// who is sending the mail ?
	CLIENT_SAYS( mail_server_socket, "MAIL FROM: <" );
	CLIENT_SAYS( mail_server_socket, from_mail );
	CLIENT_SAYS( mail_server_socket, ">\n" );
	SERVER_SAYS( mail_server_socket );

	// to who ?
	for( size_t i = 0 ; i < array_size( to ) ; i++ ) {
		CLIENT_SAYS( mail_server_socket, "RCPT TO: <" );
		CLIENT_SAYS( mail_server_socket, array_get( to, i ) );
		CLIENT_SAYS( mail_server_socket, ">\n" );
		SERVER_SAYS( mail_server_socket );
	}
	for( size_t i = 0 ; i < array_size( to_cc ) ; i++ ) {
		CLIENT_SAYS( mail_server_socket, "RCPT TO: <" );
		CLIENT_SAYS( mail_server_socket, array_get( to_cc, i ) );
		CLIENT_SAYS( mail_server_socket, ">\n" );
		SERVER_SAYS( mail_server_socket );
	}

	// mail content !
	CLIENT_SAYS( mail_server_socket, "DATA\n" );
	SERVER_SAYS( mail_server_socket );

	// header !
	CLIENT_SAYS( mail_server_socket, "Date: " );
	string date = get_time_string();
	CLIENT_SAYS( mail_server_socket, date );
	free(date);
	CLIENT_SAYS( mail_server_socket, "\n" );

	CLIENT_SAYS( mail_server_socket, "From: <" );
	CLIENT_SAYS( mail_server_socket, from_mail );
	CLIENT_SAYS( mail_server_socket, ">\n" );

	CLIENT_SAYS( mail_server_socket, "Subject: " );
	CLIENT_SAYS( mail_server_socket, subject );
	CLIENT_SAYS( mail_server_socket, "\n" );

	CLIENT_SAYS( mail_server_socket, "To: " );
	for( size_t i = 0 ; i < array_size( to ) ; i++ ) {
		CLIENT_SAYS( mail_server_socket, " " );
		CLIENT_SAYS( mail_server_socket, array_get( to, i ) );
		if( i != array_size( to ) -1 ) { CLIENT_SAYS( mail_server_socket, "," ); }
	}
	CLIENT_SAYS( mail_server_socket, "\n" );

	CLIENT_SAYS( mail_server_socket, "Cc: " );
	for( size_t i = 0 ; i < array_size( to_cc ) ; i++ ) {
		CLIENT_SAYS( mail_server_socket, " " );
		CLIENT_SAYS( mail_server_socket, array_get( to_cc, i ) );
		if( i != array_size( to_cc ) -1 ) { CLIENT_SAYS( mail_server_socket, "," ); }
	}
	CLIENT_SAYS( mail_server_socket, "\n" );

	// actual DATA !
	CLIENT_SAYS( mail_server_socket, "\n" );
	CLIENT_SAYS_FILE( mail_server_socket, path_to_mail_data );
	CLIENT_SAYS( mail_server_socket, ".\n" );
	SERVER_SAYS( mail_server_socket );

	// good bye !
	CLIENT_SAYS( mail_server_socket, "QUIT\n" );
	SERVER_SAYS( mail_server_socket );

	return 0;
}

#undef SERVER_SAYS_EHLO 
#undef SERVER_SAYS
#undef CLIENT_SAYS
#undef CLIENT_SAYS_FILE
