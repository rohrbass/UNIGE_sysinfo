#pragma once

#include "types.h"
#include "args/args.h"

/* CONNECT_SERVER_WTD -------------------------------
Establishes the connection with the server accordingly 
to port instructions given by the user. 

/!\ BEWARE /!\ : Calls connect_server internally. 
Please read BEWARE section of connect_server desc. 

IN :
WTD structure 

OUT : 
file descriptor of the server socket 
*/ 
int connect_server_wtd( WTD wtd );

/* DISCONNECT_SERVER -----------------------------------
Closes the connection
*/ 
int disconnect_server( int socket );

/* CONNECT_SERVER -----------------------------------
Establishes the connection with the server provided a 
hostname and a port 
/!\ BEWARE /!\ This function opens a socket, it needs
to be closed using socket_close (cf. socket.h)

IN : 
host
port

OUT : 
on success 
	file descriptor of the server socket 
on failure 
	error message 
*/ 
int connect_server( string host, unsigned int port );



// returns 0 if status is good
// returns -1 if status is bad

/* SERVER_SAYS --------------------------------------
Displays the status message provided by the server 

IN : 
file descriptor of the socket 

OUT : 
status
	-1 an error has occured on the server 
	0 everything is fine 
*/ 
int server_says( int socket );

/* CLIENT_SAYS --------------------------------------
Displays client's message and writes it to the socket 

IN : 
socket file descriptor 
string containing the message 

OUT :
error code 
	0 on write success 
	-1 on write failure
*/ 
int client_says( int socket, string message );

/* CLIENT_SAYS_FILE ---------------------------------
Displays client's message imported from file and
writed it to the socket 

IN : 
socket file descriptor 
path to the file containing the message 

OUT :
error code 
	0 on success 
	-1 if it experiences internal error - will be 
		printed to screen 
*/ 
int client_says_file( int socket, string path );

/* SEND_EMAIL ---------------------------------------
Sends a mail according to the smtp protocol. 
IN : 
mail_server_socket 		file descriptor of the server's socket
from_mail				sender email		
to 						reciever email(s)
to_cc					copy email(s)
subject 				string with subject of the mail
path_to_mail_data		path to the *.txt file that contains the data

OUT :
status code 
	0 on success 
	-1 on error 
*/ 
int send_email(
	int mail_server_socket,
	string from_mail,
	Array(string) to,
	Array(string) to_cc,
	string subject,
	string path_to_mail_data);


/* DO_NOT_BLOCK_ON_ERROR should be defined (or not) in the Makefile */ 
#ifdef DO_NOT_BLOCK_ON_ERROR
	#define SERVER_SAYS_EHLO( socket       ) if( server_says_ehlo( socket       ) < 0 ) {}
	#define SERVER_SAYS( socket            ) if( server_says( socket            ) < 0 ) {}
	#define CLIENT_SAYS( socket, message   ) if( client_says( socket, message   ) < 0 ) {}
	#define CLIENT_SAYS_FILE( socket, path ) if( client_says_file( socket, path ) < 0 ) {}
#else
	// if client_says or server_says do fail, stop right there and return -1
	#define SERVER_SAYS_EHLO( socket       ) if( server_says_ehlo( socket       ) < 0 ) {return -1;}
	#define SERVER_SAYS( socket            ) if( server_says( socket            ) < 0 ) {return -1;}
	#define CLIENT_SAYS( socket, message   ) if( client_says( socket, message   ) < 0 ) {return -1;}
	#define CLIENT_SAYS_FILE( socket, path ) if( client_says_file( socket, path ) < 0 ) {return -1;}
#endif


