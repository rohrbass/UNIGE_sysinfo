#include "GUI_party.h"
#include "GUI/GUI.h"
#include "utils/utils.h"

#include <math.h>
#include <complex.h>

#include <stdlib.h>

#include "utils/time.h"
#include <ncurses.h>

void GUI_fractal_loop() {
	double t = 0.;
	double t_before = get_time();
	double t_now =    get_time();
	while( 1 ) {
		t_before = t_now;
		t_now = get_time();
		double t_delta = t_now - t_before;
		t += t_delta;

		// double pixels( int x, int y, double t ) {
		// 	double value = sin( 0.01*( (x - GUI_max_x()/2 )*(y - GUI_max_y()/2) ) + t );
		// 	double color = value*.5 + .5;
		// 	return color;
		// }

		double complex pixels( int x, int y, double t ) {
			double zoom = 3;
			double complex z = .5*cos(2.7*t) + .5 * I * sin(t);
			double complex c = zoom*( x * 1. / GUI_max_x() - .5 - .2 ) + I * zoom * ( y * 1. / GUI_max_y() - .5 ) * .5 * GUI_max_y() / GUI_max_x();
			int n = 0;
			const double N_MAX = 800;
			const double Z_MAX = 100;
			while( n < N_MAX ) {
				n++;
				z = z*z + c;
				if( cabs(z) > Z_MAX )
					break;
			}
			// return 1 - abs(z)/N_MAX;
			// return 1 - cabs(z)/N_MAX;
			return cos(t) - z/Z_MAX/8;
		}

		GUI_draw_function( pixels, t );

		GUI_set( GUI_max_x() , 0, "Press 'q' to quit");

		GUI_refresh();

		char c = getch();
		if(c=='q') break;
	}
}

void launch_party() {
	GUI_init();
	GUI_fractal_loop();
	GUI_close();
}