#include "GUI.h"
#include "gl_math.h"
#include <curses.h>
#include <string.h>
#include <math.h>

void GUI_init() {
	initscr();
	cbreak();
	noecho();
	nodelay(stdscr,1);
	clear();
	start_color();
}

void GUI_clear() {
	clear();
}

void GUI_close() {
	endwin();
}

void GUI_refresh() {
	refresh();
}

int GUI_max_x() {
	return LINES-1;
}
int GUI_max_y() {
	return COLS-1;
}

void print_error_on_overflow( int x, int y ) {
	if( x < 0 || x >= LINES || y < 0 || y >= COLS ) {
		perror("GUI draw overflow x\n");
	}
}

void GUI_set_str( int x, int y, char* pixels ) {
	// print_error_on_overflow( x, y );
	mvaddstr( x, y, pixels );
}

void GUI_set_char( int x, int y, char pixel ) {
	// print_error_on_overflow( x, y );
	mvaddch( x, y, pixel );
}

void GUI_set_double( int x, int y, double pixel ) {
	const char* gradient = "$@B\%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
	int index = clamp( pixel, 0, 1 ) * strlen( gradient );
	mvaddch( x, y, gradient[ index ] );
}

void GUI_set_double_complex( int x, int y, double complex pixel ) {
	const char* gradient = "$@B\%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
	const int colorwheel[] = {
		COLOR_MAGENTA,
		COLOR_RED,
		COLOR_YELLOW,
		COLOR_GREEN,
		COLOR_CYAN,
		COLOR_BLUE
	};

	int index1 = clamp( cabs(pixel), 0, 1 ) * strlen( gradient );
	int index2 = clamp( (carg(pixel) + M_PI) / (2*M_PI), 0, 1 ) * 6;


	init_pair( 1 + index2, colorwheel[index2], COLOR_BLACK );

	attron( COLOR_PAIR( 1 + index2) );

	mvaddch( x, y, gradient[ index1 ] );

	attroff( COLOR_PAIR( 1 + index2) );

}

// void GUI_draw_function_double( double (*pixel)( int x, int y, double time ), double time ) {
// 	int max_lins = LINES;
// 	int max_cols = COLS;
// 	for( int x = 0 ; x < max_lins ; x++ ) {
// 		for( int y = 0 ; y < max_cols ; y++ ) {
// 			GUI_set( x, y, pixel( x, y, time ));
// 		}
// 	}
// }

// void GUI_draw_function_char( char (*pixel)( int x, int y, double time ), double time ) {
// 	int max_lins = LINES;
// 	int max_cols = COLS;
// 	for( int x = 0 ; x < max_lins ; x++ ) {
// 		for( int y = 0 ; y < max_cols ; y++ ) {
// 			GUI_set( x, y, pixel( x, y, time ));
// 		}
// 	}
// }

// void GUI_draw_function_str( char* (*pixel)( int x, int y, double time ), double time ) {
// 	int max_lins = LINES;
// 	int max_cols = COLS;
// 	for( int x = 0 ; x < max_lins ; x++ ) {
// 		for( int y = 0 ; y < max_cols ; y++ ) {
// 			GUI_set( x, y, pixel( x, y, time ));
// 		}
// 	}
// }

// void GUI_draw_function( double (*pixel)( int x, int y, double time ), double time ) {
// 	int max_lins = LINES;
// 	int max_cols = COLS;
// 	for( int x = 0 ; x < max_lins ; x++ ) {
// 		for( int y = 0 ; y < max_cols ; y++ ) {
// 			GUI_set( x, y, pixel( x, y, time ));
// 		}
// 	}
// }

void GUI_draw_function( double complex (*pixel)( int x, int y, double time ), double time ) {
	int max_lins = LINES;
	int max_cols = COLS;
	for( int x = 0 ; x < max_lins ; x++ ) {
		for( int y = 0 ; y < max_cols ; y++ ) {
			GUI_set( x, y, pixel( x, y, time ));
		}
	}
}

