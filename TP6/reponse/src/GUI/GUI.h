#pragma once

#include <complex.h>

void GUI_init();
void GUI_close();

void GUI_set_str( int x, int y, char* pixels );
void GUI_set_char( int x, int y, char pixel );
void GUI_set_double( int x, int y, double pixel );
void GUI_set_double_complex( int x, int y, double complex pixel );

#define GUI_set( x, y, pixel ) _Generic( ( pixel ), \
		char*: GUI_set_str, \
		char: GUI_set_char, \
		double: GUI_set_double, \
		double complex: GUI_set_double_complex, \
		default: GUI_set_double \
	)( x, y, pixel )

/*
#define GUI_draw_function( pixels_callback, t ) _Generic( ( pixels_callback ), \
	double (*callback)( int x, int y, char* t  ): GUI_draw_function_str, \
	double (*callback)( int x, int y, char t   ): GUI_draw_function_char, \
	double (*callback)( int x, int y, double t ): GUI_draw_function_double \
	)( pixels_callback, t )
*/

void GUI_draw_function( double complex (*pixel)( int x, int y, double time ), double time );
// void GUI_draw_function( double (*pixel)( int x, int y, double time ), double time );

// void GUI_draw_function_double( double (*pixel)( int x, int y, double time ), double time );

void GUI_clear();


int GUI_max_x();
int GUI_max_y();

void GUI_refresh();