#pragma once

#define min(x,y) (((x)<(y))?(x):(y))

#define UNUSED(x) (void)(x)
#define NOT_IMPLEMENTED(arg) die("NOT IMPLEMENTED : " arg)

#define MACRO_FORCE_SEMICOLON void MACRO_FORCE_SEMICOLON_PLAY_WITH_C_SYNTAX()

void die(char *issue);

void term_color_normal();
void term_color_red();
void term_color_green();
void term_color_yellow();
void term_color_blue();
void term_color_magenta();
void term_color_cyan();
void term_color_white();

// atoi that crashes on error
int better_atoi( const char* str );
long long int better_atoll( const char* str );
