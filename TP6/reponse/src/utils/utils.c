#include <stdio.h>
#include <stdlib.h>

void die( char *issue ) {
	perror(issue); 
	exit(EXIT_FAILURE); 
}

int better_atoi( const char* str ) {
	// TODO : error handling, atoi is too dumb
	return atoi( str );
}

long long int better_atoll( const char* str ) {
	// TODO : error handling, atoi is too dumb
	return atoll( str );
}

void term_color_normal() {
	printf("\x1B[0m");
}
void term_color_red() {
	printf("\x1B[31m");
}
void term_color_green() {
	printf("\x1B[32m");
}
void term_color_yellow() {
	printf("\x1B[33m");
}
void term_color_blue() {
	printf("\x1B[34m");
}
void term_color_magenta() {
	printf("\x1B[35m");
}
void term_color_cyan() {
	printf("\x1B[36m");
}
void term_color_white() {
	printf("\x1B[37m");
}
