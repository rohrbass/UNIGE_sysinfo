#pragma once

#include "utils/utils.h"
/* 
 * a macro that makes an array for any type
 * usage example :

	#import "any_arrays.h"

	typedef struct yourStuff yourStuff;

	// in .h
	prototype_array_of( yourStuff );

	// in .c
	implement_array_of( yourStuff );

	int main() {
		Array(yourStuff) a = new_array(yourStuff);

		array_push( a, 5 ); 
		array_push( a, 10 );

		array_get( a, 0);
		array_set( a, 0, 2);

		array_free(a);
	}

*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define using_array_of( TYPE ) \
typedef struct Array_##TYPE Array_##TYPE;\
typedef Array_##TYPE* Array_ptr_##TYPE;\
struct Array_##TYPE {\
	TYPE *content;\
	size_t _used;\
	size_t _size;\
	void (*push)( Array_##TYPE *self, TYPE element );\
	TYPE (*pop) ( Array_##TYPE *self );\
	TYPE (*get) ( Array_##TYPE *self, size_t position );\
	TYPE (*set) ( Array_##TYPE *self, size_t position, TYPE element );\
	void (*free) ( Array_##TYPE **self );\
	size_t (*size) ( Array_##TYPE *self );\
};\
MACRO_FORCE_SEMICOLON

// typedef void* ___TEMPLATE___;
// using_array_of( ___TEMPLATE___ )

// #define array_push( a, element  ) ((Array_ptr____TEMPLATE___)a)->push( a, element )
// #define array_size( a )           ((Array_ptr____TEMPLATE___)a)->size( a )
// #define array_get(  a, position ) ((Array_ptr____TEMPLATE___)a)->get ( a, position )
// #define array_set(  a, position ) ((Array_ptr____TEMPLATE___)a)->set ( a, position, element )
// #define array_free( a           ) ((Array_ptr____TEMPLATE___)a)->free( &a )

#define array_push( a, element  ) a->push( a, element )
#define array_pop(  a )           a->pop ( a )
#define array_size( a )           a->size( a )
#define array_get(  a, position ) a->get ( a, position )
#define array_set(  a, position ) a->set ( a, position, element )
#define array_free( a           ) a->free( &a )



#define new_array( TYPE ) new_array_##TYPE()
#define Array( TYPE ) Array_ptr_##TYPE

#define prototype_array_of( TYPE )\
using_array_of( TYPE );\
typedef struct Array_##TYPE Array_##TYPE;\
typedef Array_##TYPE* Array_ptr_##TYPE;\
struct Array_##TYPE;\
void array_push_##TYPE( Array_##TYPE *a, TYPE element );\
TYPE array_pop_##TYPE( Array_##TYPE *a );\
TYPE array_get_##TYPE( Array_##TYPE * a, size_t position );\
size_t array_size_##TYPE( Array_##TYPE * a  );\
TYPE array_set_##TYPE( Array_##TYPE * a, size_t position, TYPE new_value );\
void free_array_##TYPE( Array_##TYPE **a );\
Array_##TYPE* new_array_##TYPE()\


#define implement_array_of( TYPE )\
/*using_array_of(TYPE)*/\
void array_push_##TYPE( Array_##TYPE *a, TYPE element ) {\
	if( a == NULL) {\
		fprintf( stderr, "ERROR : cannot push element in uninitialized array" );\
		exit( EXIT_FAILURE );\
	}\
	if( a->_used == a->_size ) {\
		a->_size *= 2;\
		a->content = (TYPE *)realloc( a->content, a->_size * sizeof(TYPE) );\
	}\
	a->content[a->_used++] = element;\
}\
\
TYPE array_pop_##TYPE( Array_##TYPE *a ) {\
	if( a == NULL) {\
		fprintf( stderr, "ERROR : cannot pop element in uninitialized array" );\
		exit( EXIT_FAILURE );\
	}\
	if( a->_used == 0 ) {\
		fprintf( stderr, "ERROR : cannot pop element in empty array" );\
		exit( EXIT_FAILURE );\
	}\
	return a->content[a->_used-- - 1];\
}\
\
TYPE array_get_##TYPE( Array_##TYPE * a, size_t position ) {\
	if( position >= a->_size ) {\
		fprintf( stderr, "ERROR : cannot access element outside of array" );\
		exit( EXIT_FAILURE );\
	}\
	return a->content[position];\
}\
\
size_t array_size_##TYPE( Array_##TYPE * a  ) {\
	return a->_used;\
}\
\
TYPE array_set_##TYPE( Array_##TYPE * a, size_t position, TYPE new_value ) {\
	TYPE old_value = array_get_##TYPE( a, position );\
	a->content[position] = new_value;\
	return old_value;\
}\
\
void free_array_##TYPE( Array_##TYPE **a ) {\
	free((*a)->content);\
	(*a)->content = NULL;\
	(*a)->_used = 0;\
	(*a)->_size = 0;\
	free(*a);\
	(*a)=NULL;\
}\
\
Array_##TYPE* new_array_##TYPE() {\
	Array_##TYPE* a = malloc( sizeof( Array_##TYPE ) );\
	size_t initial_size = 32;\
	a->content = (TYPE *)malloc( initial_size * sizeof( TYPE ) );\
	a->_used = 0;\
	a->_size = initial_size;\
	a->push = array_push_##TYPE;\
	a->pop = array_pop_##TYPE;\
	a->get = array_get_##TYPE;\
	a->set = array_set_##TYPE;\
	a->free = free_array_##TYPE;\
	a->size = array_size_##TYPE;\
	return a;\
}\
MACRO_FORCE_SEMICOLON

// map that outputs something
#define array_map( TYPE, OUTPUT_TYPE ) \
array_map_##TYPE##_______##OUTPUT_TYPE

#define allow_array_map( TYPE, OUTPUT_TYPE ) \
Array(OUTPUT_TYPE) array_map_##TYPE##_______##OUTPUT_TYPE(\
	Array(TYPE) a,\
	OUTPUT_TYPE (*callback) (\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	)\
) {\
	Array(OUTPUT_TYPE) out = new_array(OUTPUT_TYPE);\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		array_push( out, callback( array_get( a, i ) , i, a ) );\
	}\
	return out;\
}\
MACRO_FORCE_SEMICOLON

// map that does not output anything
#define array_map_void( TYPE ) \
array_map_##TYPE

#define allow_array_map_void( TYPE ) \
void array_map_##TYPE (\
	Array(TYPE) a,\
	void (*callback) (\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	)\
) {\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		callback( array_get( a, i ) , i, a );\
	}\
}\
MACRO_FORCE_SEMICOLON

#define array_reduce( TYPE, OUTPUT_TYPE ) \
array_reduce_##TYPE##_______##OUTPUT_TYPE

#define allow_array_reduce( TYPE, OUTPUT_TYPE ) \
OUTPUT_TYPE array_reduce_##TYPE##_______##OUTPUT_TYPE(\
	Array(TYPE) a,\
	OUTPUT_TYPE (*callback) (\
		OUTPUT_TYPE accumulator,\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	),\
	OUTPUT_TYPE accumulator_initial_value\
) {\
	OUTPUT_TYPE out = accumulator_initial_value;\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		out = callback( out, array_get( a, i ) , i, a );\
	}\
	return out;\
}\
MACRO_FORCE_SEMICOLON

// TODO : 

// OUTPUT_TYPE reduce(OUTPUT_TYPE)(
// 	Array(TYPE) a,
// 	OUTPUT_TYPE (*callback) (
// 		TYPE current_element,
// 		OUTPUT_TYPE accumulator,
// 		Array(TYPE) a,
// 		size_t current_index
// 	),
// 	OUTPUT_TYPE accumulator_initial_value
// );

