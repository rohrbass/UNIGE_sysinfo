#include <sys/time.h>
#include <stddef.h>

double get_time() {
	struct timeval currentTime;
	gettimeofday( &currentTime, NULL );
	// return currentTime.tv_sec + currentTime.tv_usec * 1e-6;
	return currentTime.tv_sec + currentTime.tv_usec*1e-6;
}
