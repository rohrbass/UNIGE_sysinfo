#include "args.h"

void print_help( string binary_name ) {
	printf(
	"usage :\n"
	"  %s [-h] <host> <path/to/mail> [-p <custom port>] [-r/c <recipient>] [-s <sender>] [-S <subject>]\n"
	"\n"
	"Option :\n"
	"  -s : email source                 (required)\n"
	"  -S : subject                      (required)\n"
	"  -r : add a email destination      (need at least one -r or -c)\n"
	"  -c : add a email destination (cc) (need at least one -r or -c)\n"
	"  -p : custom port                  (by default will try 25, 465 and 587)\n"
	"  -P : make a big party             (recommended)\n"
	"\n"
	"Argument order :\n"
	"  <host> must be specified before <path/to/mail>\n"
	"\n"
	"Example :\n"
	"  %s smtp.youporn.gov ~/.ssh/id_rsa -r first@email.sexy -r second@email.sexy -c president@email.sexy -s me@me.net -S \"mail subject\"\n"
	, binary_name, binary_name );
}

WTD parse_arguments( int argc, char *argv[] ) {
	/* initialise WhatToDo structure */ 
	WTD wtd;
	wtd.action = 0;
	wtd.host = NULL;
	wtd.port = 0;
	wtd.path_to_mail = NULL;
	wtd.sender = NULL;
	wtd.to    = new_array(string);
	wtd.to_cc = new_array(string);

	int opt;
	int arg_counter = 0;
	/* get arguments */ 
	while( ( opt = getopt( argc, argv, "-p:Phc:r:s:S:") ) != -1 ) {
		switch( opt ) {
			case 'h' :
				print_help( argv[0] );
				exit( EXIT_SUCCESS );
				break;
			case 'P' :
				wtd.action |= ACTION_PARTY;
				break;
			case 'p' :
				wtd.action |= ACTION_CUSTOM_PORT;
				wtd.port = atoi(optarg);
				break;
			case 'r' :
				array_push( wtd.to, optarg );
				break;
			case 'c' :
				array_push( wtd.to_cc, optarg );
				break;
			case 's' :
				wtd.sender = optarg;
				break;
			case 'S' :
				wtd.subject = optarg;
				break;
			default :
				arg_counter++;
				switch( arg_counter ) {
					case 1:
						wtd.host = optarg;
						break;
					case 2:
						wtd.path_to_mail = optarg;
						break;
					default:
						break;
				}
		}
	}

	int arg_missing = (
		wtd.sender == NULL ||
		wtd.subject == NULL ||
		wtd.host == NULL ||
		wtd.path_to_mail == NULL ||
		((array_size(wtd.to)==0) && (array_size(wtd.to_cc)==0))
	); 

	if( !( wtd.action & ACTION_PARTY ) && arg_missing ) {
		/* wtd.action is equal to either 0 or ACTION_CUSTOM_PORT */ 
		print_help( argv[0] );
		exit( EXIT_FAILURE );
	}
 
	return wtd;

}

void wtd_delete( WTD wtd ) {
	array_free( wtd.to );
	array_free( wtd.to_cc );
}
