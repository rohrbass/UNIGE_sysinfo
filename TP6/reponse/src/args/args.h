#pragma once

#include "types.h"

#define ACTION_PARTY 1<<0
#define ACTION_CUSTOM_PORT 1<<1

typedef struct{
	string host;
	unsigned int port;
	string path_to_mail;
	Array(string) to;
	Array(string) to_cc;
	string sender;
	string subject;
	int action;
} WTD;

WTD parse_arguments( int argc, char *argv[] );
void wtd_delete( WTD wtd );
