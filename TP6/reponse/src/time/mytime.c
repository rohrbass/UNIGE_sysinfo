#include "mytime.h"
#include <time.h>
#include <string.h>

string time_to_str( time_t t ) {
	const size_t DATE_MAX_LENGTH = 1024;
	char *date = malloc( DATE_MAX_LENGTH * sizeof( DATE_MAX_LENGTH ) );
	memset( date, 0, DATE_MAX_LENGTH );

	struct tm *tmp = localtime(&t);
	if (tmp == NULL) {
		perror("ERROR mytime : localtime\n");
		return "";
	}

	// CLIENT_SAYS( mail_server_socket, "Thu, 21 May 2008 05:33:29 -0700" );

	if ( strftime(date, DATE_MAX_LENGTH, "%a, %d %b %Y %T %z", tmp) == 0 ) {
			fprintf(stderr, "ERROR mytime : strftime failed\n");
			return "";
	}

	return date;
}

string get_time_string() {
	time_t now = time(NULL);
	return time_to_str(now);
}
