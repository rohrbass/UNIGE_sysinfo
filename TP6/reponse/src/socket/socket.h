#pragma once
#include "types.h"

/* SOCKET_OPEN -----------------------------------------
Tries to open a stream socket for the hostname provided 
in input. 

IN : 
	hostname
	port 

OUT :
	the socket's file descriptor on success 
	-1 on internam failure
*/ 
int socket_open( string host, int port );

/* SOCKET_CLOSE ----------------------------------------
Closes the socket opened by socket_open and displays if 
an error has occured.

IN : 
	file descriptor of the socket 

OUT : 
error code : 
	0 on success 
	-1 otherwise
*/ 
int socket_close( int socket );





/* SOCKET_READ -----------------------------------------
Reads in the socket provided as an argument and returns
what it has read.

IN : 
	socket file descriptor 

OUT :
	socket content in a type "string" format. (see types.h 
		for details on "string")
*/ 
string socket_read( int socket );

/* SOCKET_READ_AND_PRINT -------------------------------
IN : 
OUT : 
*/ 
string socket_read_and_print( int socket, string pre );

int socket_write( int socket, string buffer, ssize_t buffer_size );

/* SOCKET_WRITE_STRING ----------------------------------------
Write a message to the socket 

IN : 
	socket file descriptor 
	string message to swite
OUT : 
error code : 
	-1 write failed or did not entirely write 
	0 success
*/ 
int socket_write_string( int socket, string message );

/* SOCKET_WRITE_FILE -----------------------------------
Writes a message from a file to a socket

IN : 
	socket file descriptor 
	path to the file which contains the text we want to send

OUT :
error code : 
	-1 if an internal error has occured 
	0 success 
*/ 
int socket_write_file( int socket, string path_to_file );
