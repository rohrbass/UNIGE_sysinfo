#include "socket.h"
#include "utils/utils.h"

#include <arpa/inet.h>
#include <errno.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <unistd.h>

int socket_open( string host, int port ) {

	/* Obtain address(es) matching host/port */
	struct addrinfo hints;
	memset( &hints, 0, sizeof( struct addrinfo ) );
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_STREAM; /* Two-way stream */
	hints.ai_flags = 0;
	hints.ai_protocol = 0;          /* Any protocol */

	struct addrinfo *result;
	int s = getaddrinfo( host, NULL, &hints, &result );
	if (s != 0) {
		// fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		// a simpler message
		fprintf( stderr, "could not resolve %s ", host );
		return -1;
	}

	/* getaddrinfo() returns a list of address structures.
		Try each address until we successfully connect(2).
		If socket(2) (or connect(2)) fails, we (close the socket
		and) try the next address. */
	int socket_fd = -1;
	struct addrinfo *rp;
	for( rp = result; rp != NULL; rp = rp->ai_next ) {
		socket_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if ( socket_fd == -1 ){ continue; } /* try an other one */

		int synRetries = 2; // Send a total of 3 SYN packets => Timeout ~7s
		setsockopt( socket_fd , IPPROTO_TCP, TCP_SYNCNT, &synRetries, sizeof(synRetries));

		((struct sockaddr_in *)rp->ai_addr)->sin_port = htons(port);
		if (connect(socket_fd, rp->ai_addr, rp->ai_addrlen) != -1)
			break;                  /* Success */

		close( socket_fd );
	}
	if ( rp == NULL ) {             /* No address succeeded */
		fprintf(stderr, "Could not connect ");
		return -1;
	}

	freeaddrinfo( result );
	return socket_fd;
}

string socket_read( int socket ) {
	// TODO : read chunks and combine

	/* define a buffer for reading */ 
	const size_t BUF_SIZE = 4096;
	char buf[BUF_SIZE];
	memset(buf,0,BUF_SIZE);

	/* read and handle its errors */ 
	ssize_t nread = read( socket, buf, BUF_SIZE );
	if (nread == -1) {
		perror("ERROR read_socket failed");
		return NULL;
	}

	/* return pointer to the string read from socket*/ 
	size_t total_read = nread;
	string out = malloc( total_read * sizeof(char) );
	out[0]='\0';
	strcpy( out, buf );

	return out;
}

string socket_read_and_print( int socket, string pre ) {
	string out = socket_read( socket );
	printf( "%s%s", pre, out );
	return out;
}

int socket_write_file( int socket, string path_to_file ) {
	/* open the file we want to read in */ 
	int file_fd = open( path_to_file, O_RDONLY );
	if( file_fd < 0 ){
		fprintf( stderr, "ERROR could not open '%s'\n", path_to_file );
		return -1;
	}

	const size_t BUFFER_SIZE = 32;
	char buffer[BUFFER_SIZE+1];
	ssize_t nread;

	while( ( nread = read( file_fd, buffer, BUFFER_SIZE ) ) != 0 ) {
		if( nread == -1 ) {
			fprintf( stderr, "ERROR could not read '%s'\n", path_to_file );
			close( file_fd );
			return -1;
		}
		buffer[ nread ] = '\0';
		printf("%s",buffer);fflush(stdout);
		if( socket_write( socket, buffer, nread ) < 0 ) {
			return -1;
		}
	}

	close( file_fd );
	return 0;

}

/* // version envoi en un bloc
int socket_write_string( int socket, string message ) {
	// TODO : write by smaller chunks
	ssize_t len = strlen( message ) ;
	if( write( socket, message, len) != len ) {
		fprintf(stderr, "partial/failed write\n");
		return -1;
	}
	return 0;
}
*/
int socket_write_string( int socket, string message ) {
	ssize_t bytes_written = 0; 
	size_t total_written = 0; 
	size_t len = strlen( message ); 
	const size_t block_size = 10; // arbitrary value 
	while( (bytes_written = write( socket, message + total_written, min(len, block_size))) >0 ) {
		len -= bytes_written; 
		total_written += bytes_written; 
	}
	if( bytes_written == -1 ) {
		perror("write internal error"); 
		return -1; 
	}
	return 0;
}

int socket_write( int socket, string buffer, ssize_t buffer_size ) {
	// TODO : write by smaller chunks
	if( write( socket, buffer, buffer_size) != buffer_size ) {
		fprintf(stderr, "partial/failed write\n");
		return -1;
	}
	return 0;
}

int socket_close( int socket ) {
	int err = close(socket); 
	if(err<0) 
	{
		perror("Error on socket closing"); 
		return -1;
	}
	return 0;
}

