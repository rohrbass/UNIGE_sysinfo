#include "GUI/GUI_party.h"
#include "utils/utils.h"
#include "args/args.h"
#include "socket/socket.h"
#include "types.h"
#include <stdlib.h>
#include <getopt.h>

#include "mail/server.h"

int main( int argc, char *argv[] ) {

	WTD wtd = parse_arguments( argc, argv );

	// this is the most important thing.
	if( wtd.action & ACTION_PARTY ) {
		launch_party();
		return 0;
	}

	int mail_server_socket = connect_server_wtd( wtd );

	int status = send_email (
		mail_server_socket,
		wtd.sender,
		wtd.to,
		wtd.to_cc,
		wtd.subject,
		wtd.path_to_mail
	); 

	/* free up ressources */ 
	disconnect_server( mail_server_socket );
	wtd_delete( wtd );
	return status;
}
