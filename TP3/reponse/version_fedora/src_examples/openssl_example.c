#include <stdio.h>
#include <openssl/evp.h>
#include <string.h>

int main( int argc, char *argv[] ) {
	EVP_MD_CTX * md_context;
	const EVP_MD *md;
	char mess[] = "une phrase";
	unsigned char md_value[EVP_MAX_MD_SIZE];
	unsigned int md_len, i;

	if( !argv[1] ) {
		printf("Usage: mdtest digestname\n");
		exit(1);
	}

	md = EVP_get_digestbyname(argv[1]);

	if( !md ) {
		printf("Unknown message digest %s\n", argv[1]);
		exit(1);
	}

	// create context
	md_context = EVP_MD_CTX_new();

	EVP_DigestInit_ex( md_context, md, NULL );
	EVP_DigestUpdate( md_context, mess, strlen(mess) ); 
	EVP_DigestFinal_ex( md_context, md_value, &md_len );

	// delete context
	EVP_MD_CTX_free( md_context );

	printf("Digest is: ");
	for (i = 0; i < md_len; i++) {
		// printf("%02x", md_value[i]);
		printf("%02d", md_value[i]);
	}
	printf("\n");

	exit(0);

}
