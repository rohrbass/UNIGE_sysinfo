#pragma once

/* 
 * a macro that makes an array for any type
 * usage example :

	#import "any_arrays.h"

	typedef struct yourStuff yourStuff;

	// in .h
	prototype_array_of( yourStuff );

	// in .c
	implement_array_of( yourStuff );
	// or 
	using_array_of( yourStuff );

	int main() {
		Array(yourStuff) a = new_array(yourStuff);

		array_push( a, 5 ); 
		array_push( a, 10 );

		array_get( a, 0);
		array_set( a, 0, 2);

		array_free(a);
	}

*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define using_array_of( TYPE ) \
typedef struct Array_##TYPE Array_##TYPE;\
typedef Array_##TYPE* Array_ptr_##TYPE;\
struct Array_##TYPE {\
	TYPE *content;\
	size_t _used;\
	size_t _size;\
	void (*push)( Array_##TYPE *self, TYPE element );\
	TYPE (*get) ( Array_##TYPE *self, size_t position );\
	TYPE (*set) ( Array_##TYPE *self, size_t position, TYPE element );\
	void (*free) ( Array_##TYPE **self );\
	size_t (*size) ( Array_##TYPE *self );\
};


// this is a terrible hack that I didn't really want to get in.
//
// so anyone reading this piece of art is probably nose bleeding and wondering
// how the flippin hell this happened.
//
// here are some explanations :
// I wanted to be able to make an arry of any types without having to copy paste
// the same code for each type. So I thought, let's write macros that do that job
// for me.
// it worked quite well until I had to split the project into different files.
// Then I needed each structures to exist on each files, but I can't "ifndef"
// inside a define macro. So I can't declare my structs only once.
// Then I came to the realization that the size of the structure is not dependent
// of the TYPE (we have pointers and size_t) which is good news because that means
// that I sould be able to declare a struct with the same element in the same
// order, and should be able to cast my custom_typed array into that template_array !
// 
// The only thing is that I have to cross my fingers if the compiler tries to be
// smart and optimize things in those struct and does not reorder stuff in that
// struct !!!
// 
// so far it is working (except the type checker wich makes me sad), and I will never
// try anything like this with C.
// in short... given that situation here, I could have make use of a void* array with
// only the correct functions for each time (so that sizeof(TYPE) works correctly in
// each mallocs)

// typedef void* ___TEMPLATE___;
// using_array_of( ___TEMPLATE___ )

// #define array_push( a, element  ) ((Array_ptr____TEMPLATE___)a)->push( a, element )
// #define array_size( a )           ((Array_ptr____TEMPLATE___)a)->size( a )
// #define array_get(  a, position ) ((Array_ptr____TEMPLATE___)a)->get ( a, position )
// #define array_set(  a, position ) ((Array_ptr____TEMPLATE___)a)->set ( a, position, element )
// #define array_free( a           ) ((Array_ptr____TEMPLATE___)a)->free( &a )

#define array_push( a, element  ) a->push( a, element )
#define array_size( a )           a->size( a )
#define array_get(  a, position ) a->get ( a, position )
#define array_set(  a, position ) a->set ( a, position, element )
#define array_free( a           ) a->free( &a )



#define new_array( TYPE ) new_array_##TYPE()
#define Array( TYPE ) Array_ptr_##TYPE

#define prototype_array_of( TYPE )\
typedef struct Array_##TYPE Array_##TYPE;\
typedef Array_##TYPE* Array_ptr_##TYPE;\
struct Array_##TYPE;\
void array_push_##TYPE( Array_##TYPE *a, TYPE element );\
TYPE array_get_##TYPE( Array_##TYPE * a, size_t position );\
size_t array_size_##TYPE( Array_##TYPE * a  );\
TYPE array_set_##TYPE( Array_##TYPE * a, size_t position, TYPE new_value );\
void free_array_##TYPE( Array_##TYPE **a );\
Array_##TYPE* new_array_##TYPE()


#define implement_array_of( TYPE )\
using_array_of(TYPE)\
void array_push_##TYPE( Array_##TYPE *a, TYPE element ) {\
	if( a == NULL) {\
		fprintf( stderr, "ERROR : cannot push element in uninitialized array" );\
		exit( EXIT_FAILURE );\
	}\
	if( a->_used == a->_size ) {\
		a->_size *= 2;\
		a->content = (TYPE *)realloc( a->content, a->_size * sizeof(TYPE) );\
	}\
	a->content[a->_used++] = element;\
}\
\
TYPE array_get_##TYPE( Array_##TYPE * a, size_t position ) {\
	if( position >= a->_size ) {\
		fprintf( stderr, "ERROR : cannot access element outside of array" );\
		exit( EXIT_FAILURE );\
	}\
	return a->content[position];\
}\
\
size_t array_size_##TYPE( Array_##TYPE * a  ) {\
	return a->_used;\
}\
\
TYPE array_set_##TYPE( Array_##TYPE * a, size_t position, TYPE new_value ) {\
	TYPE old_value = array_get_##TYPE( a, position );\
	a->content[position] = new_value;\
	return old_value;\
}\
\
void free_array_##TYPE( Array_##TYPE **a ) {\
	free((*a)->content);\
	(*a)->content = NULL;\
	(*a)->_used = 0;\
	(*a)->_size = 0;\
	free(*a);\
	(*a)=NULL;\
}\
\
Array_##TYPE* new_array_##TYPE() {\
	Array_##TYPE* a = malloc( sizeof( Array_##TYPE ) );\
	size_t initial_size = 32;\
	a->content = (TYPE *)malloc( initial_size * sizeof( TYPE ) );\
	a->_used = 0;\
	a->_size = initial_size;\
	a->push = array_push_##TYPE;\
	a->get = array_get_##TYPE;\
	a->set = array_set_##TYPE;\
	a->free = free_array_##TYPE;\
	a->size = array_size_##TYPE;\
	return a;\
}

// map that outputs something
#define array_map( TYPE, OUTPUT_TYPE ) \
array_map_##TYPE##_______##OUTPUT_TYPE

#define allow_array_map( TYPE, OUTPUT_TYPE ) \
Array(OUTPUT_TYPE) array_map_##TYPE##_______##OUTPUT_TYPE(\
	Array(TYPE) a,\
	OUTPUT_TYPE (*callback) (\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	)\
) {\
	Array(OUTPUT_TYPE) out = new_array(OUTPUT_TYPE);\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		array_push( out, callback( array_get( a, i ) , i, a ) );\
	}\
	return out;\
}

// map that does not output anything
#define array_map_void( TYPE ) \
array_map_##TYPE

#define allow_array_map_void( TYPE ) \
void array_map_##TYPE (\
	Array(TYPE) a,\
	void (*callback) (\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	)\
) {\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		callback( array_get( a, i ) , i, a );\
	}\
}











#define array_reduce( TYPE, OUTPUT_TYPE ) \
array_reduce_##TYPE##_______##OUTPUT_TYPE

#define allow_array_reduce( TYPE, OUTPUT_TYPE ) \
OUTPUT_TYPE array_reduce_##TYPE##_______##OUTPUT_TYPE(\
	Array(TYPE) a,\
	OUTPUT_TYPE (*callback) (\
		OUTPUT_TYPE accumulator,\
		TYPE current_element,\
		size_t current_index,\
		Array(TYPE) current_array\
	),\
	OUTPUT_TYPE accumulator_initial_value\
) {\
	OUTPUT_TYPE out = accumulator_initial_value;\
	for( size_t i = 0 ; i < array_size(a) ; i++ ) {\
		out = callback( out, array_get( a, i ) , i, a );\
	}\
	return out;\
}




// OUTPUT_TYPE reduce(OUTPUT_TYPE)(
// 	Array(TYPE) a,
// 	OUTPUT_TYPE (*callback) (
// 		TYPE current_element,
// 		OUTPUT_TYPE accumulator,
// 		Array(TYPE) a,
// 		size_t current_index
// 	),
// 	OUTPUT_TYPE accumulator_initial_value
// );

