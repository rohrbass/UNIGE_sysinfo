// hash functions
#include <openssl/evp.h>
#include <string.h>

#include "hash.h"

using_array_of( string )
using_array_of( uchar )


Array( uchar ) hash_from_callback( void (*callback)(EVP_MD_CTX *md_context), string hash_function ) {
	Array( uchar ) out = new_array(uchar);

	const EVP_MD *message_digest = EVP_get_digestbyname( hash_function );
	if( !message_digest ) {
		printf( "Unknown message digest %s\n", hash_function );
		exit( EXIT_FAILURE );
	}

	// create context
	EVP_MD_CTX *md_context = EVP_MD_CTX_new();
	EVP_DigestInit_ex( md_context, message_digest, NULL );

	// calculate hash
	callback( md_context );

	// output hash
	unsigned char md_value[EVP_MAX_MD_SIZE];
	unsigned int md_len;
	EVP_DigestFinal_ex( md_context, md_value, &md_len );

	// delete context
	EVP_MD_CTX_free( md_context );

	for( size_t i = 0; i < md_len; i++ ) {
		array_push( out, md_value[i] );
	}
	return out;
}


Array( uchar ) hash_from_path( string path, string hash_function ) {
	void __hash__file( EVP_MD_CTX *md_context ) {

		FILE *file = fopen( path, "r" ); // read mode

		if ( file == NULL ) {
			fprintf(stderr, "Error while opening \"%s\".\n" , path);
			exit(EXIT_FAILURE);
		}

		size_t bytes_read = 0;
		unsigned char buffer[65536];
		while ( ( bytes_read = fread(buffer, 1, sizeof(buffer), file ) ) > 0) {
			EVP_DigestUpdate( md_context, buffer, bytes_read );
			// process bytesRead worth of data in buffer
		}

		fclose( file );
	}
	return hash_from_callback( __hash__file, hash_function );

}



Array( uchar ) hash_from_string( string msg, string hash_function ) {
	void __hash__string( EVP_MD_CTX *md_context ) {
		EVP_DigestUpdate( md_context, msg, strlen(msg) );
	}
	return hash_from_callback( __hash__string, hash_function );
}

// void print_hash_array( Array( uchar ) hash , string append) {
// 	for( size_t i = 0 ; i < array_size( hash ) ; i++ ) {
// 		printf( "%02x", array_get( hash, i ) );
// 	}
// 	printf( "   %s\n", append );
// }

string hash_array_to_string( Array( uchar ) hash ) {
	size_t size = 2 * array_size( hash );
	string out = malloc(size * sizeof(char));
	out[0] = '\0';

	for( size_t i = 0 ; i < array_size( hash ) ; i++ ) {
		sprintf( out, "%s%02x", out , array_get( hash, i ) );
	}
	return out;
}
