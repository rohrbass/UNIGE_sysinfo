#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "types.h"
#include "hash.h"
#include "argument_parse.h"
#include "utils.h"

using_array_of( string )
using_array_of( uchar )

implement_array_of( size_t )

allow_array_map( string, size_t );

void test_arrays() {
	Array( string ) a = new_array( string );
	array_push( a, "coucou" );
	array_push( a, "monde" );
	size_t func( string element, size_t i, Array( string ) array ) {
		return 0;
	}
	Array(size_t) out  = array_map(string, size_t) ( a , func );

}

allow_array_reduce( string, int );
int more_test_arrays() {
	Array( string ) a = new_array( string );
	array_push( a, "coucou" );
	array_push( a, "monde" );
	int func( int acc, string element, size_t i, Array( string ) array ) {
		return acc + strlen(element);
	}
	return array_reduce( string, int )( a , func , 0 );
}
