#pragma once

#include "types.h"

Array(uchar) hash_from_string( string msg, string hash_function );
// void print_hash_array( Array( uchar ) hash , string append);
string hash_array_to_string( Array( uchar ) hash );
Array( uchar ) hash_from_path( string path, string hash_function );