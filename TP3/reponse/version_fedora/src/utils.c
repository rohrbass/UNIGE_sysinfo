#include "utils.h"
#include <string.h>

using_array_of( string )
using_array_of( uchar )

string array_to_string( Array(string) string_array  ) {

	size_t msg_size = 1; // we need at least '\0'
	string msg = malloc( msg_size * sizeof( char ) );
	msg[0] = '\0';

	// si on a au moins un mot, le mettre
	if( array_size( string_array ) ) {
		string to_copy = array_get( string_array , 0 );
		msg_size += strlen(to_copy);
		msg = realloc( msg, msg_size * sizeof( char ) );

		strcpy( msg, to_copy );
	}
	// ajouter les autres mots séparés d'un espace
	for ( size_t i = 1 ; i < array_size( string_array ) ; i++ ) {
		string to_copy = array_get( string_array , i );

		msg_size += 1; // space
		msg_size += strlen(to_copy); // the actual word
		msg = realloc( msg, msg_size * sizeof( char ) );

		strcat( msg, " " );
		strcat( msg, to_copy );
	}
	return msg;
}
