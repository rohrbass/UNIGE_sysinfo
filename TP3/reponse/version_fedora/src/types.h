#pragma once

#include <unistd.h>

#define true 1
#define false 0

typedef char* string;
typedef unsigned char uchar;

// ==============================
// ===   make custom arrays   ===
// ==============================

#include "any_arrays.h"

prototype_array_of( string );
prototype_array_of( uchar );
prototype_array_of( char );
