#include "argument_parse.h"
#include <getopt.h>
#include <string.h>

using_array_of( string )
using_array_of( uchar )

What_to_do parse_args( int argc, char *argv[] ) {
	What_to_do out;

	// default mode is message mode
	out.message_mode = true;

	// hash_function is SHA1 by default
	out.hash_function = "sha1";

	// the content is empty at the beginning
	// TODO : ! memory leak ! should implement free_What_to_do
	out.files_or_message = new_array(string);

	{
		int opt;
		while ( ( opt = getopt( argc, argv, "-ht:f" ) ) != -1 ) {
			switch ( opt ) {
				case 'h':
					printf( "usage : %s [-h] [-t hash_function] [-f] words...\n", argv[0] );
					printf( " -h           : prints this message\n" );
					printf( " -t hash_func : sets the hash function\n" );
					printf( " -f           : if set, words are interpreted as file paths\n" );
					exit(EXIT_SUCCESS);
					break;
				case 't':
					// TODO : should copy or not ?
					out.hash_function = optarg;
					break;
				case 'f':
					out.message_mode = false;
					break;
				case 1:
					// if is not an invalid option
					if( optarg ) {
						array_push( out.files_or_message, optarg );
					}
					break;
			}
		}
	}

	return out;
}
