<style>.newpage {page-break-before: always;}</style>

# Array template in C !

This project features a custom typed array code automation macro-cluster-fuck-party™


## usage example 
```c
// file : my_custom_arrays.h
#pragma once

#include "any_arrays.h"

typedef char* string;
typedef unsigned char uchar;
typedef struct YourStruct YourStruct {
	// whatever you want
} 

prototype_array_of( char );
prototype_array_of( int );
prototype_array_of( string );
prototype_array_of( uchar );
prototype_array_of( YourStruct );

// we cannot have characters such as
// ' ' or '*' in out name thus are the
// typedefs above
```


```c
// file : my_custom_arrays.c
#include "my_custom_arrays.h"

implement_array_of( char );
implement_array_of( int );
implement_array_of( string );
implement_array_of( uchar );
implement_array_of( YourStruct );
```

```c
// file : main.c
#include "my_custom_arrays.h"

// must be on each .c that uses a certain type of array, this is
// due to some complicated stuff with impossible nested macros.
using_array_of( char );
using_array_of( int );
using_array_of( string );
using_array_of( uchar );
using_array_of( YourStruct );

int main() {
	Array( uchar ) my_array = new_array( uchar ); // creates a new array of uchar, this allocates memory

	uchar element = 'r';
	array_push( my_array, element ); // this pushes back an element inside the array.

	// loop inside the array
	for( size_t i = 0 ; i < array_size( my_array ) ; i++ ) {
		uchar element = array_get( my_array, i ); // access an element
		array_set( my_array, i , 'c' ); // change an element
	}

	array_free( my_array ); // dispose
}
```

<div class="newpage"></div>

## API
``` c
Array(TYPE) // this is an array type

Array(TYPE) new_array(TYPE); // this creates an array containing element of type TYPE
void array_free( Array(TYPE) a ); // this frees the array a (a is also set to NULL)

size_t array_size ( Array(TYPE) a ); // size of the array a

TYPE array_get ( Array(TYPE) a, size_t position); // access 'position'th element
TYPE array_set ( Array(TYPE) a, size_t position, TYPE element); // changes 'position'th element and returns previous value

void array_push( Array(TYPE) a ); // push_back
TYPE array_pop ( Array(TYPE) a ); // pop_back (TODO)

void array_push_front( Array(TYPE) a ); // (TODO)
TYPE array_pop_front ( Array(TYPE) a ); // (TODO)
```

## going further

`map` and `reduce` .

```c
// this enables array_map/array_map_void/array_reduce for the wished types
allow_array_map_void( T      );
allow_array_map     ( T, OUT );
allow_array_reduce  ( T, OUT );

void       array_map_void( T      )(Array(T) a, void(*callback)(T el, size_t index, Array(T) a));
Array(OUT) array_map     ( T, OUT )(Array(T) a, OUT (*callback)(T el, size_t index, Array(T) a));
OUT        array_reduce  ( T, OUT )(Array(T) a, OUT (*callback)( OUT acc, T el, size_t i, Array(T) a), OUT acc_init );
```

usage example

```c
// implement required arrays types
implement_array_of( T );
implement_array_of( OUT );

allow_array_map_void( T      );
allow_array_map     ( T, OUT );
allow_array_reduce  ( T, OUT );

void func() {
	Array(T) a = new_array(T);
	T element1, element2, element3;
	array_push(a, element1);
	array_push(a, element2);
	array_push(a, element3);

	void mapping_cb_1(T element, size_t index, Array(T) a) {
		do_something(element);
	}
	array_map_void( T )( a, mapping_cb_1 );

	OUT mapping_cb_2(T element, size_t index, Array(T) a) {
		return do_something_else(element);
	}
	Array(OUT) mapped = array_map( T, OUT )( a, mapping_cb_2 );

	int reducer( int accumulator, T element, size_t index, Array(T) a) {
		return accumulator + calculate(element);
	}
	int accumulation = array_reduce( T, int )( a, reducer, 0 );
}
```

<div class="newpage"></div>

## how the hell does that thing work ?

`Array(TYPE)` is a macro that makes a `Array_ptr_##TYPE` which is a `Array_##TYPE *` (a pointer to Array_##TYPE).
For example, `Array(int)` will become `Array_ptr_int`.

`prototype_array_of(TYPE)` and `implement_array_of(TYPE)` define the same functions and structures, thow `prototype_array_of(TYPE)` doesn't implement anything. This is because I can't guarantee that each implementation for each type is called once and only once. (otherwise, we coult just `implement_array_of(TYPE)` whenever we need an array of a new type).

However `any_arrays.h` also provide practical macros such as `array_size(a)` that simpler code. Without it, things would be more tedious. example :
```c
	Array(int)          array_1 = new_array(int);
	Array(MyCustomType) array_2 = new_array(MyCustomType);
	size_t size_1;
	size_t size_2;

	// any_arrays makes those functions
	size_1 = array_size_int( array_1 );
	size_2 = array_size_MyCustomType( array_2 );

	// those same functions have their address written in the array,
	// so that we can avoid to type the type in the function name
	size_1 = array_1->size( array_1 );
	size_2 = array_2->size( array_2 );

	// but this opens to this kind of error wich may be valid to the compiler
	// but no to what the user wanted to do.
	size_2 = array_2->size( an_other_array );

	// with our macro, we have this (*)
	size_1 = array_size( array_1 );
	size_2 = array_size( array_2 );
```
However `(*)` requires the full declaration of the structure `Array_##TYPE` which has to be done on each separate `*.c` file (because of a mess with macros that don't allow the `#ifndef` single inclusion mechanism inside a `#define`).

