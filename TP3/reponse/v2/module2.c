/* ce module ce charge de hasher le texte qui lui est donné */ 

#include "module2.h" 

int hash_text(int argc, char * argv[], int * n, const EVP_MD * md)
{

	/* concatenate the strings together */ 
	char texte_entier[4096]; 
	int n_left = argc - *n; 
	for(int i = 0; i<n_left; i++)
	{
		strcat(texte_entier, argv[*n + i]); 
		if(i<(n_left-1))
		{
			strcat(texte_entier, " "); 
		}
	}
	
	printf("Original text : %s \n", texte_entier); 

	/* initialise the digest context */ 
	EVP_MD_CTX * mdctx = EVP_MD_CTX_create(); 
	EVP_DigestInit_ex(mdctx, md, NULL); 

	/* hash */ 
	EVP_DigestUpdate(mdctx, texte_entier, strlen(texte_entier)); 

	/* terminate the digest context */ 
	unsigned char md_value[EVP_MAX_MD_SIZE]; 
	unsigned int md_len; 
	EVP_DigestFinal_ex(mdctx, md_value, &md_len); 
	EVP_MD_CTX_destroy(mdctx); 

	/* display the obtained digest */ 
	printf("Digest is : "); 
	for(int i = 0; i<md_len; i++)
	{
		printf("%02x", md_value[i]); 
	}
	printf("\n"); 

	return 0; 
}




int hash_file(char file_name[], const EVP_MD * md)
{
	/* initialise the digest context */ 
	EVP_MD_CTX * mdctx = EVP_MD_CTX_create(); 
	EVP_DigestInit_ex(mdctx, md, NULL); 

	/* get file */ 
	unsigned char buffer[65536]; 
	size_t bytes_read = 0; 
	FILE * file = fopen(file_name, "r"); 

	/* test if the file is proper */ 
	if(file==NULL)
	{
		printf("Error while opening \"%s\". \n", file_name); 
		return -2; 
	}

	/* hash */ 
	while( (bytes_read = fread(buffer, 1, sizeof(buffer), file)) >0 )
	{
		EVP_DigestUpdate(mdctx, buffer, bytes_read); 
	}

	/* close file */ 
	fclose(file); 

	/* terminate the digest context */ 
	unsigned char md_value[EVP_MAX_MD_SIZE]; 
	unsigned int md_len; 
	EVP_DigestFinal_ex(mdctx, md_value, &md_len); 
	EVP_MD_CTX_destroy(mdctx); 

	/* display the obtained digest in hexadecimal */ 
	for(int i = 0; i<md_len; i++)
	{
		printf("%02x", md_value[i]); 
	}
	printf("\t %s \n", file_name); 

	return 0; 
}




int hash(int argc, char * argv[], bool * message_mode, char digest_type[], int * n)
{
	/* *********** set up environement *********** */ 

	/* initialise the program */ 
	OpenSSL_add_all_digests(); 
	const EVP_MD * md = EVP_get_digestbyname(digest_type); 

	/* test if proper digest_type */ 
	if(!md) 
	{
		printf("Unknown digest \"%s\". \n", digest_type); 
		return -1; 
	}
	/* ******************************************** */

	

	/* ************ do the hashing **************** */ 
	int out1; /* for error code */ 

	if(*message_mode==true)
	{
		out1 = hash_text(argc, argv, n, md); 
	}
	else
	{
		int n_left = argc - *n; 
		for(int i = 0; i<n_left; i++) 
		{
			out1 = hash_file(argv[*n+i], md); /* process */ 
		}
	}
	
	/* ******************************************** */



	/* *************** clean up ******************* */ 

	EVP_cleanup(); 
	return out1; 
	/* ******************************************** */
}
