#include <stdio.h> 

#include "module1.h" 
#include "module2.h"


int main(int argc, char * argv[])
{
	/* ETAPE 1 ********************************* */ 
	/* initialisation des variables pour la gestions des options */ 
	int out1, n; 
	bool message_mode; 
	char hash_function[20]; 

	/* gérer les options */ 
	out1 = getoptions(argc, argv, &message_mode, hash_function, &n); 
	
	/* gestion des erreurs de getoptions */ 
	switch (out1)
	{
		case -2: 
			/* mauvais nombre de paramètres */ 
			return -1; 

		case -1: 
			/* mauvais paramètres entrés dans la fonction */ 
			return -1; 
		
		case 1: 
			/* le menu help a été affiché */ 
			return 0; 

		default:
			/* tout c'est déroulé correctement, continuer */ 
			break; 
	}
	/* ******************************************* */ 
	
	/* faire part à l'utilisateur des options choisies */ 
	printf("text mode : %d, hash function : %s\n\n", message_mode, hash_function); 
	
	/* ETAPE 2 : hashage ************************* */ 
	/* initialiser les variables pour le hashage */ 
	int out2; 

	/* appeller la fonction de hachage */ 
	out2 = hash(argc, argv, &message_mode, hash_function, &n); 
	printf("\n"); 

	/* gestion des erreurs de la fonction de hachage */ 
	switch (out2) 
	{
		case 0: 
			/* déroulement correct */ 
			return 0; 

		case -1: 
			/* wrong digest message */ 
			return -1;

		case -2: 
			/* empty file provided */ 
			return -1;
	}
}
