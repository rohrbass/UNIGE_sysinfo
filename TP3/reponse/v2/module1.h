#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 

#ifndef MODULE2
/* défninir un type booléen */ 
typedef int bool; 
enum {false, true};
#endif 

#ifndef MODULE1
#define MODULE1

/* GETOPTIONS
*** WHAT IT DOES : 
gets the options provided by the user to the function hash and stores the 

*** IN : 
argc 			count of the arguments passed to the main function of hash.c 
argv			arguments passed to the main function of hash.c 
message_mode 	0 if we are expecting a text string, 1 if we expect file names
hash_function 	the name of the hash function we want to use to compute digests
n				(technical) where we stopped reading arguments, so the module 2 can
				parse string inputs / filename inputs.

*** OUT : error message : 
1				Function just displayed help
0 				Terminated correctly
-1 				User misspecification in "hash" function input 
-2 				No arguments specified by the user (no text / no filenames)
*/

int getoptions(int argc, char * argv[], bool * message_mode, char hash_function[], int * n); 


#endif
