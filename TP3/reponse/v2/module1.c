/* Ce module ce charge de traiter les arguments entrants */ 

#include "module1.h"

int getoptions(int argc, char * argv[], bool * message_mode, char hash_function[], int * n)
{
	/* set default values */
	strcpy(hash_function, "sha1"); /* hash_function est un string vide */ 
	* message_mode = true;
	
	/* get options */ 
	int opt; 
	while((opt = getopt(argc, argv, "ht:f")) != -1)
	{
		switch (opt) 
		{
			case 'h': 
				/* help */
				printf( "usage : %s [-h] [-t hash_function] [-f] words...\n", argv[0] ); 
				printf( " -h           : prints this message\n" );               
				printf( " -t hash_func : sets the hash function\n" );            
				printf( " -f           : if set, words are interpreted as file paths\n" );
				return 1; 
			
			case 't': 
				/* choice of hash function */ 
				if(20>=strlen(optarg))
				{
					strcpy(hash_function, optarg); 
				}
				else
				{
					fprintf(stderr, "%s : Invalid hashing function - too long", optarg);
					return -3; 
				}
				break; 

			case 'f': 
				/* choice of operating mode */ 
				* message_mode = false; 
				break; 

			default: 
				/* other flags */ 
				fprintf(stderr, "Usage: %s [-h] [-t hash function] [-f] words ... \n", argv[0]); 
				return -1; 
		}
	}

	/* test if arguments have been passed to the function */ 
	if(optind>=argc)
	{
		fprintf(stderr, "Expected argument after options \n"); 
		return -2; 
	}

	*n = optind; /* number of parameters passed that need to be processed by our program */ 

	return 0; 
}
