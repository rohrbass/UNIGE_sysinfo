#include <stdio.h> 
#include <openssl/evp.h> 
#include <string.h> 

#ifndef MODULE1
typedef int bool; 
enum {false, true}; 
#endif

#ifndef MODULE2
#define MODULE2

/* HASH_TEXT
*** WHAT IT DOES : 
Computes hash of a whole string. 

*** IN : 
argc			count of the arguments passed by the main function of hash.c
argv			arguments passed to the main function of hash.c
n 				(technical) where we stopped reading arguments in module1, 
				to parse string inputs. 
md				digest value provided by the function HASH of module 2 (see below).

*** OUT : error messages : 
0 				Terminated correctly
*/
int hash_text(int argc, char * argv[], int *n, const EVP_MD * md); 



/* HASH_FILE 
*** WHAT IT DOES : 
Computes the hash of a single file. 

*** IN : 
file_name 		The name of the file we want to hash 
md 				digest value provided by the function HASH of module 2 (see below).

*** OUT : 
0 				Terminated correctly 
-2 				Error happened while opening the file
*/
int hash_file(char file_name[], const EVP_MD * md); 



/* HASH 
*** WHAT IT DOES : 
Checks if the digest type is proper and calls HASH_FILE or HASH_TEXT 
depending on the provided mode of operation (message_mode) 

*** IN : 
argc 			count of the arguments passed to the main function of hash.c 
argv 			arguments passed to the main function of hash.c 
message_mode 	0 if we expect a text string, 1 if we expect file names
digest_type 	the name of the hash function we want to use to compute digests
n 				(technical) where we stopped reading the arguments in module 1, 
				so we can parse string inputs / filename inputs. 

*** OUT : 
0				Terminated correctly
-1 				Unknown digest 
other 			See functions above
*/
int hash(int argc, char * argv[], bool * message_mode, char digest_type[], int * n );

#endif
