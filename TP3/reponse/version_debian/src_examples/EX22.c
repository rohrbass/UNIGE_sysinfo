#include <stdio.h> 
/* librairie pour les hash */ 
#include <openssl/evp.h> 

main(int argc, char *argv[])
{

	/* INITIALISATION OF VARIABLES */ 

	/* pointer to context */
	EVP_MD_CTX *mdctx;
	
	/* pointer on structure for digest  */ 
	const EVP_MD *md; 
	
	/* texts à etre hashés */ 
	char mess1[] = "Test Message\n"; 
	char mess2[] = "Hello World\n"; 
	
	/* variable pour contenir le digest */ 
	unsigned char md_value[EVP_MAX_MD_SIZE]; 
	
	/* longeur du digest md_value et ... */ 
	int md_len, i; 
	/* ************************************************** */ 



	/* INITIALISE THE PROGRAM  */ 
	/* adds all digest algorithms to the internal table
		void parameter 
		Used by many parts of the library, 
		not including it might cause functions to missbehaveand complain */ 
	OpenSSL_add_all_digests(); 

	/* error if argument not provided */ 
	if(!argv[1]) 
	{
		printf("Usage: mdtest digestname\n");
		exit(1); 
	}

	/* need to be initialised by OpenSSL_add_all_digests()
		returns the EVP_MD structure, that is understood by the program */ 
	md = EVP_get_digestbyname(argv[1]); 

	if(!md)
	{
		printf("Unknown message digest %s\n", argv[1]); 
		exit(1); 
	}

	/* *************************************************** */



	/* DO THE HASHING */
	/* allocates, initializes and returns digest context 
		Matches : EVP_MD_CTX_destroy() */ 
	mdctx = EVP_MD_CTX_create(); 
	
	/* EVP_DigestInit_ex(digest context, digest type, implementation of digest type) 
		--> initialises the digest context
		/!\ context must be initialized before this call /!\
		type = NULL -> default implementation*/
	EVP_DigestInit_ex(mdctx, md, NULL); 

	/* EVP_DigestUpdate(digest context, text, bit length) 
		Rq : we can do multiple calls in the same context */
	EVP_DigestUpdate(mdctx, mess1, strlen(mess1)); 
	EVP_DigestUpdate(mdctx, mess2, strlen(mess2)); 
	
	/* retrieves the digest from context (mdctx), 
		places it in ... 
		/!\ EVP_DigestUpdate cannot be called anymore, 
			need to call EVP_DigestInit_ex before to do so/!\ */
	EVP_DigestFinal_ex(mdctx, md_value, &md_len); 
	
	/* cleans digest context ctx and frees up the allocated space 
		Matches : EVP_MD_CTX_create()*/
	EVP_MD_CTX_destroy(mdctx); 
	/* ***************************************************** */ 



	/* DISPLAY THE OBTAINED DIGEST IN HEXADECIMAL */ 
	printf("Digest is: "); 
	for(i = 0; i < md_len; i++)
	{
		/* x stands for unsigned hexadecimal notation */ 
		printf("%02x", md_value[i]); 
	}
	printf("\n"); 
	/* ***************************************************** */ 


	/* CLEAN UP */ 
	/* removes all digest from the table, 
		goes with the OpenSSL_add_all_digests() */ 
	EVP_cleanup(); 
	exit(0); 
	/* **************************************************** */ 
}
