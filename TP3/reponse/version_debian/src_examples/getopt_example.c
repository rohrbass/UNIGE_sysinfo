#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

int main(int argc, char *argv[]) {
	int flags = 0;
	int seconds = 0;
	int t_found = 0;

	{
		int opt;
		while ((opt = getopt(argc, argv, "nt:")) != -1) {
			switch (opt) {
				case 'n':
					flags = 1;
					break;
				case 't':
					seconds = atoi(optarg);
					t_found = 1;
					break;
				default: /* '?' */
					fprintf(stderr, "Usage: %s [-t seconds] [-n] name\n", argv[0]);
					exit(EXIT_FAILURE);
			}
		}
	}

	printf("flags=%d; t_found=%d; seconds=%d; optind=%d\n",
		flags, t_found, seconds, optind);

	if (optind >= argc) {
		fprintf(stderr, "Expected argument after options\n");
		exit(EXIT_FAILURE);
	}

	printf("name argument = %s\n", argv[optind]);

	/* Other code omitted */

	exit(EXIT_SUCCESS);
}
