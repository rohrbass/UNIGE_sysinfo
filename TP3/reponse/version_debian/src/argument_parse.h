#pragma once

#include "types.h"

typedef struct {
	Array( string ) files_or_message;
	int message_mode;
	string hash_function;
} What_to_do;

What_to_do parse_args( int argc, char *argv[] );
