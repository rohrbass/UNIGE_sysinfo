#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "types.h"
#include "hash.h"
#include "argument_parse.h"
#include "utils.h"
#include <openssl/evp.h>

using_array_of( string )
using_array_of( uchar )

allow_array_map_void( string );

static string BRICOLAGE_LAST_MINUTE_HASH_FUN;

void func( string path, size_t i, Array( string ) array ) {
	Array( uchar ) hash = hash_from_path( path, BRICOLAGE_LAST_MINUTE_HASH_FUN );
	printf("%s   %s\n", hash_array_to_string( hash ), path );
	array_free( hash );
}

int main( int argc, char *argv[] ) {

	// what shall we do ?
	What_to_do wtd = parse_args( argc, argv );
	hash_init();

	// hash message or file
	if ( wtd.message_mode ) {
		string msg = array_to_string( wtd.files_or_message );

		Array( uchar ) hash = hash_from_string( msg, wtd.hash_function );

		printf("%s   %s\n", hash_array_to_string( hash ), msg );

		array_free( hash );

	} else {
	// 	BRICOLAGE_LAST_MINUTE_HASH_FUN = wtd.hash_function;

	// 	array_map_void( string ) ( wtd.files_or_message , func );
	}

	exit(EXIT_SUCCESS);
}
