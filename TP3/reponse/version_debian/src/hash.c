// hash functions
#include <openssl/evp.h>
#include <string.h>

#include "hash.h"

using_array_of( string )
using_array_of( uchar )


#define BRICOLAGE_LAST_MINUTE_A(hash_function) \
	Array( uchar ) out = new_array(uchar);\
	const EVP_MD *message_digest = EVP_get_digestbyname( hash_function );\
	if( !message_digest ) {\
		printf( "Unknown message digest %s\n", hash_function );\
		exit( EXIT_FAILURE );\
	}\
	EVP_MD_CTX *md_context = EVP_MD_CTX_create();\
	EVP_DigestInit_ex( md_context, message_digest, NULL );

#define BRICOLAGE_LAST_MINUTE_B() \
	unsigned char md_value[EVP_MAX_MD_SIZE];\
	int md_len;\
	EVP_DigestFinal_ex( md_context, md_value, &md_len );\
\
	EVP_MD_CTX_destroy( md_context );\
\
	for( size_t i = 0; i < md_len; i++ ) {\
		array_push( out, md_value[i] );\
	}\
	EVP_cleanup();\
	return out

void hash_init() {
	OpenSSL_add_all_digests();
}


Array( uchar ) hash_from_path( string path, string hash_function ) {
	BRICOLAGE_LAST_MINUTE_A( hash_function );
		FILE *file = fopen( path, "r" ); // read mode

		if ( file == NULL ) {
			fprintf(stderr, "Error while opening \"%s\".\n" , path);
			exit(EXIT_FAILURE);
		}

		size_t bytes_read = 0;
		unsigned char buffer[65536];
		while ( ( bytes_read = fread(buffer, 1, sizeof(buffer), file ) ) > 0) {
			EVP_DigestUpdate( md_context, buffer, bytes_read );
			// process bytesRead worth of data in buffer
		}

		fclose( file );
	BRICOLAGE_LAST_MINUTE_B();

}



Array( uchar ) hash_from_string( string msg, string hash_function ) {
	BRICOLAGE_LAST_MINUTE_A( hash_function );
		EVP_DigestUpdate( md_context, msg, strlen(msg) );
	BRICOLAGE_LAST_MINUTE_B();
}

// void print_hash_array( Array( uchar ) hash , string append) {
// 	for( size_t i = 0 ; i < array_size( hash ) ; i++ ) {
// 		printf( "%02x", array_get( hash, i ) );
// 	}
// 	printf( "   %s\n", append );
// }

string hash_array_to_string( Array( uchar ) hash ) {
	size_t size = 2 * array_size( hash );
	string out = malloc(size * sizeof(char));
	out[0] = '\0';

	for( size_t i = 0 ; i < array_size( hash ) ; i++ ) {
		char toread[3];
		sprintf( toread, "%02d", array_get( hash, i ) );
		strcat( out, toread );
		// sprintf( out, "%s%s", out , toread );
		// sprintf( out, "%s%02d", out , array_get( hash, i ) );
		// sprintf( out, "%s%02d", out , "hello" );
	}
	return out;
}
