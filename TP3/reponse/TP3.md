# exo 2.1. hash

```shell
cat phrase.txt
# outputs
# "une phrase"
# with no \n

md5sum phrase.txt
cat phrase.txt | md5sum
echo -n une phrase | md5sum

sha1sum phrase.txt
cat phrase.txt | sha1sum
echo -n une phrase | sha1sum

```
Chaque bloque donne le même hash

# exo 2.2. openssl

nothing to say