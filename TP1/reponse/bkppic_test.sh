#~/bin/bash

# manual test

if [ "$(./bkppic source target)" ]; then
	echo "[OK]  : failed with success"
	echo 
fi

# automated tests (unfinished)

# setup some cases
tmp=$(mktemp -d)
chmod a+rw "$tmp"


SOURCE=$tmp/source
TARGET=$tmp/target
TARGET_NOT_EMPTY=$tmp/target_not_empty
mkdir "$SOURCE"
mkdir "$TARGET"
mkdir "$TARGET_NOT_EMPTY"


touch $SOURCE/not_a_picture
convert -size 100x100 xc:white "$SOURCE"/picture.jpg
convert -size 100x100 xc:white "$TARGET_NOT_EMPTY"/picture.jpg

# TODO : SOURCE is a symlink
# TODO : TARGET is a symlink
# TODO : TARGET is a symlink that points to SOURCE

# symlinks
# pathological case
ln -s $tmp/target $tmp/source
# case : relative points in
# TODO
# case : relative points out
# TODO
# case : absolute points in
# TODO
# case : absolute points out
# TODO


function print_err_or_ok() {
	if [ $1 -eq 0 ]; then
		echo -n "[OK]  : "
	else
		echo -n "[ERR] : "
	fi
}

function should_fail() {
	command_to_run=$1
	eval $command_to_run > /dev/null 2>&1 
	state=$?
	# state = not state
	if [ -z $state ];then
		state=1
	else
		state=0
	fi
	print_err_or_ok $state 
}

function should_succeed() {
	command_to_run=$1
	eval $command_to_run > /dev/null 2>&1 
	state=$?
	print_err_or_ok $state 
}


# no args
should_fail "./bkppic"
echo "no argument must fail"

# missing one argument
should_fail "./bkppic $SOURCE"
echo "missing one argument must fail"


should_fail "./bkppic $SOURCE $SOURCE"
echo "\"source = target\" must fail"

echo -n "[TODO]: "
echo "source does not exist"

echo -n "[TODO]: "
echo "source is not a directory"

echo -n "[TODO]: "
echo "target is not a directory"

echo -n "[TODO]: "
# should_??? "./bkppic $SOURCE $TARGET_NOT_EMPTY"
echo "target non empty ?"

should_succeed "./bkppic $SOURCE $TARGET"
echo "regular case does not crash"

echo -n "[TODO]: "
echo "check if result is coherent"

echo -n "[TODO]: "
echo "symlink ?"

echo -n "[TODO]: "
echo "target do conflict after regex"


echo -n "[TODO]: "
echo "source directory contain annoyoing characters such as space"
