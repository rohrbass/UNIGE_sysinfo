<style>.newpage {page-break-before: always;}</style>

# 2.2 Commande `echo`


Quelle est la fonction de la commande echo ?

Quels sont les paramètres de cette commande ?
`-n` n'envoie pas un caractère de retour à la ligne avant de terminer.
`-e` active l'interprétation de certains caractères suivi de `\`.
`-E` désactive l'interprétation de certains caractères suivi de `\`.
`--help` affiche une partie de la man page.
`--version` affiche la version du programme echo.

|code | interprétation avec le mode `-e` activé|
|-|-|
|\\      | backslash|
|\a      | alert (BEL)|
|\b      | backspace|
|\c      | produce no further output|
|\e      | escape|
|\f      | form feed|
|\n      | new line|
|\r      | carriage return|
|\t      | horizontal tab|
|\v      | vertical tab|
|\0NNN   | byte with octal value NNN (1 to 3 digits)|
|\xHH    | byte with hexadecimal value HH (1 to 2 digits)|


Attention : peut être que le shell provide sa propre implémentation d'`echo` qui pourrait différer du comportement du binaire echo fourni par le système. Cela semble être le cas pour `zsh`.

Sur l'implémentation de `zsh`, il n'y a ni `--help`, ni `--version`. Aussi `-e` est activé par défaut.

Exemple : 
```shell
echo -e "bonjour \a"
# affiche "bonjour " + le caractère bell.
# sur certains terminaux, ça va faire blip

echo -E "bonjour \a"
# affiche "bonjour \a"
```

<div class="newpage"></div>

# Créer un fichier

Créer un fichier vide :
```shell
# normalement
touch mon_super_fichier

# j'ai besoin d'un fichier maintenant,
# le nom est sans importance
# je veux juste pas écraser d'autres fichiers
tmpfile=$(mktemp)

# en utilisant le cours
echo -n > mon_super_fichier
cat /dev/null > mon_super_fichier

# the "stackoverflow + copy/paste" style
dd if=/dev/random of=mon_super_ficher bs=1 count=0

# the google style
systemctl start dropbox
espeak "ok google,
make a file named mon_super_ficher into my dropbox"
```

En somme, il y a beaucoup de façon de créer un fichier vide avec UNIX.

<div class="newpage"></div>

# 2.4 Nautilus

```
nautilus
```
ça ouvre un file manager

```
nautilus PATH
```
ça ouvre un file manager dans le dossier PATH


# 2.5 Fichiers

``` shell
wc -w < foo.txt
cat foo.txt | wc -w
wc -w foo.txt
```

`wc -w < foo.txt` Ça met le fichier foo.txt dans stdin de `wc`. C'est le shell qui "vomit" le contenu de `foo.txt` dans stdin. Aussi, wc n'est pas exécuté si `foo.txt` ne peut pas être ouvert.

`cat foo.txt | wc -w`, ici c'est `cat` qui "vomit" le contenu de foo.txt dans stdout, qui est "branché" au stdin de `wc` (qui remange tout en somme).

`wc -w foo.txt` Ça passe en argument le nom du fichier que `wc` va ouvrir lui même.


# 2.6 écrire un programme

