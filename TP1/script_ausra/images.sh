#!/bin/bash

# paramètres du script
REP_DEP=$1
REP_ARR=$2
RESOLUTION=${3:-1} #facteur d'aggrandissement 

function createRep 
{
	if [ ! -d $REP_ARR ]; then 
		# creer le directoire manquant (ainsi que ses directoires parents) 
		# notifier l'utilisateur d'un tel changement
		mkdir -vp $REP_ARR
	
	else 
		echo "Directory $REP_ARR already exists"
	fi
} 

function copierImages 
{		
	for file in *.*; do

		# verifier si c'est une image
		# A sera vide si ce n'est pas le cas 
		A=(`file --mime-type "$file" | grep "image"`) #| cut -d ":" -f1`)

		# si A est non-vide, on copie le fichier (file est une image)
		if [ ! -z "$A" ]; then 
	
			echo "Copying file : "$file" "
			cp "$file" $REP_ARR 
		fi
	done
}

function reformatNom
{
	for file in *.*; do 

		# enlever consécutivement les espaces, les guillets, les points d'interogation
		# garder le nouveau nom dans la variable locale name
		local name=`echo $file | tr -d \ | tr -d \" | tr -d \?`
		
		if [ ! "$file" == "$name" ] && [ -f "$file" ]; then 
			echo "Rename : "$file" to "$name" "
			
			# on renome le fichier en le bougeant sur un fichier qui s'appelle name (le nouveau nom)
			mv "$file" "$name"
		fi
	done
}

function changerResolution
{
	for file in *.*; do

		# remove extension 
		local B=`echo "$file" | cut -d "." -f1`

		# changer la résolution 
		if [ -f "$file" ] && [ ! "$file" == $B.png ]; then 
			
			if [ $RESOLUTION == 1 ]; then
			# par défaut on ne converti pas la taille 
				convert "$file" $B.png

			else 
			# si on spécifie une résolution, redimentionner
				convert -resize $RESOLUTION "$file" $B.png
			fi

			# supprimer l'ancien fichier
			rm "$file"
		fi
	done
}



# creer le répertoire l'arrivé s'il n'existe pas 
createRep

# se mettre dans le répertoire de départ 
cd $REP_DEP

# copier les images dans le nouveau répertoire
copierImages 

# se mettre dans le répertoire d'arrivée
cd $REP_ARR

# enlever les espaces, apostrophes, guillements des noms de fichier
reformatNom 

# changer la résolution des images et toutes les mettre au format .png 
changerResolution

