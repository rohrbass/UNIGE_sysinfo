#include "gimme_random.h"

#include <errno.h> 
#include <fcntl.h> 
#include <stdio.h> 

int fill_buffer( char *buffer, size_t buffer_size ) {

	/* create a file descriptor for /dev/urandom */ 
	int fd = open( "/dev/urandom", O_RDONLY );
	if( fd < 0 ) {
		perror( "ERROR fill_buffer : could not open /dev/urandom\n" );
		return -1;
	}

	/* write bytes from /dev/urandom to the buffer */ 
	int nRead;
	while( ( nRead = read( fd, buffer, buffer_size ) ) > 0 ) {
		buffer_size -= nRead; 
	}
	if( nRead < 0 ) {
		perror( "ERROR fill_buffer : read error\n" );
		return -1;
	}

	/* free ressources */ 
	close( fd );

	return 0;
}
