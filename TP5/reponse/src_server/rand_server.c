#include "rand_server.h"
#include "gimme_random.h"
#include "common.h"

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 

/* buffer size for copying values from /dev/urandom */ 
#define BUFF_SIZE 4096

int handle_client( int sock_client ) {

	/* bytes to send to the client */ 
	size_t nAsked = 0; 
	/* bytes sent to the client */ 
	unsigned int nSent = 0; 

	/* get the number of bytes to return to the client */ 
	int nRead = read( sock_client, &nAsked, sizeof( nAsked ) ); 
	if(nRead<0) {
		perror("ERROR handle_client : didn't received wanted size\n"); 
		return -1; 
	}

	/* define the buffer for reading random bytes */ 
	char buffer[BUFF_SIZE];
	if( fill_buffer( buffer, BUFF_SIZE ) < 0 ) { return -1; }

	/* send nAsked random bytes*/
	while( nSent < nAsked ) {
		ssize_t left_to_read = nAsked - nSent;

		// send the maximum we can send (either everything, either finish somewhere in the buffer)
		ssize_t to_send = min( BUFF_SIZE, left_to_read );

		if( write( sock_client, buffer, to_send ) != to_send ) {
			perror( "ERROR handle_client : could not send the noise !" ); 
			return -1; 
		}
		nSent += to_send;

		// refill if needed
		if( left_to_read > BUFF_SIZE ) {
			if( fill_buffer( buffer, BUFF_SIZE ) < 0 ) { return -1; }
		}
	}

	return 0;
}
