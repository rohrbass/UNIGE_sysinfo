#include "rand_server.h"
#include "socket.h"
#include "common.h"

#include <unistd.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
// #include <sys/socket.h> 
// #include <sys/types.h> 
// #include <sys/stat.h> 
// #include <fcntl.h> 
// #include <errno.h> 

#include <signal.h>

/* maximum client queue */ 
#define MAX_PENDING 5

volatile sig_atomic_t exit_program = 0;

volatile int socket_server;

/* AUXILIARY FUNCTIONS --------------------------------------------------------- */ 

void terminate_loop( int signum ) {
	UNUSED( signum );
	printf( "\n\ncaught a signal, closing server socket\n" );
	// TODO : 
	// exit_program = 1;
	// force `accept` to fail to unlock
	close_socket( socket_server ); 
	exit(EXIT_SUCCESS);
}

void main_loop( int sock_serv ) {
	while( !exit_program ) {
		/* defines client socket */ 
		struct sockaddr_in client_addr;
		unsigned int client_len = sizeof(client_addr); 

		printf( "waiting for new client ... " );
		fflush( stdout );
		int sock_client = accept( sock_serv, (struct sockaddr *) &client_addr, &client_len );
		if( sock_client < 0 ) {
			die( "Failed to accept client connection\n" );
		}

		printf( "Client connected: %s\n", inet_ntoa(client_addr.sin_addr));

		if( handle_client( sock_client ) < 0 ) {
			fprintf( stderr, "ERROR handle_client : client_handling failed !\n" );
		}

		close( sock_client ); 
	}
}

/* MAIN ------------------------------------------------------------------------------------ */
/* ----------------------------------------------------------------------------------------- */ 

int main( int argc, char *argv[] ) {

	/* initialise */ 
	if( argc != 2 ) {
		fprintf(stderr, "USAGE: %s <port>\nNumber of arguments passed : %d\n", argv[0], argc-1); 
		return -1; 
	}
	int port = better_atoi(argv[1]); 

	/* create the server socket */ 
	socket_server = create_socket(port); // crashed on failure /* TODO : ??? */ 

	/* listen for incomming connections */ 
	if (listen( socket_server, MAX_PENDING ) < 0) {
		die( "Failed to listen on server socket" );
	}

	signal( SIGINT, terminate_loop );

	/* handle clients */ 
	main_loop( socket_server );

	/* terminate */ 
	close_socket( socket_server ); 
	exit(EXIT_SUCCESS);
}
