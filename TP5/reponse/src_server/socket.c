#include "socket.h"
#include "common.h"
#include <string.h> 
#include <unistd.h> 

/* PREPARE_ADDRESS ---------------------------------------
Fills in the structure fields of sockaddr_in 
IN : 
	address : pointer to the structure we need to fill
	port    : port on which the communication will be occuring
*/
int prepare_address( struct sockaddr_in *address, int port ) {

	/* initialise the memory allocated to address to 0 */ 
	memset( address, 0, sizeof( struct sockaddr_in ) );

	// could be AF_INET instead of PF_INET cuz of historic things
	address->sin_family = AF_INET; 

	// Adresse IP auto, lie le socket à toutes les interfaces
	address->sin_addr.s_addr = htonl(INADDR_ANY);

	// convert host to network byte order
	address->sin_port = htons(port);

	// TODO : handle eventual errors
	return 0;
}

int create_socket(int port) {

	/* prepare address */ 
	/* initialise the structure */ 
	struct sockaddr_in serv_addr; 
	prepare_address( &serv_addr, port );

	/* create server socket */
	int sock_serv = socket(PF_INET, SOCK_STREAM, 0); /* sock_serv is a file descriptor */ 
	if( sock_serv<0 ) {
		die("Failed to create the server socket");
	}

	/* bind the address with the socket */ 
	if( bind( sock_serv, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0  ) {
		die("Failed to bind the server socket");
	}

	return sock_serv; 
}

void close_socket( int socket ) {
	close( socket ); 
}
