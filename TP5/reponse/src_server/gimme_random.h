#pragma once

#include <unistd.h> 

/* FILL_BUFFER -------------------------------------
 * Remplit le buffer avec des données de /dev/urandom 
 * IN : 
 *   buffer : pointeur sur le buffer 
 *   buffer_size : la taille du buffer
 * OUT : 
 *   code d'erreur (ou pas) pour l'execution 
 */ 
int fill_buffer( char *buffer, size_t buffer_size );
