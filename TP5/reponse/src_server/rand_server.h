#pragma once

/* HANDLE_CLIENT -----------------------------------------
creates a client socket, defines a buffer, reads from /dev/urandom
to the buffer, writes from the buffer to the client's socket 
IN : 
sock_client			file descriptor of the clients socket 
*/
int handle_client(int sock_client);  
