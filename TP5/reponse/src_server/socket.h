#pragma once
#include <arpa/inet.h> /*inet_pton, htons */ 

/* CREATE_SOCKET ----------------------------------------- 
Creates the server socket and binds it to the server's address
IN : 
	port : port on which the communication will take place
*/
int create_socket( int port ); 

/* CLOSE_SOCKET  ----------------------------------------- 
Closes the server socket created by create_socket
IN : 
	socket : the socket to be closed
*/
void close_socket( int socket );
