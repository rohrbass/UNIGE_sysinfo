#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h> 

#include "common.h"

/* Prépare l'adresse du serveur */
void prepare_address( struct sockaddr_in *address, const char *host, int port ) {
	size_t addrSize = sizeof( address );
	memset(address, 0, addrSize);
	address->sin_family = AF_INET;
	// inet_pton( AF_INET, (char*) address, &(address->sin_addr) );
	inet_pton( AF_INET, host, &(address->sin_addr) );
	address->sin_port = htons(port);
}

/* Construit le socket client */
int makeSocket( const char *host, int port ) {
	int sock = socket(PF_INET, SOCK_STREAM, 0);
	if( sock < 0 ) {
		die("Failed to create socket");
	}
	struct sockaddr_in address;
	prepare_address( &address, host, port );
	if( connect( sock, (struct sockaddr *) &address, sizeof( address ) ) < 0) {
		die("Failed to connect with server");
	}
	return sock;
}

int main(int argc, char *argv[]) {

	/* Initialisation */
	if (argc != 4) {
		fprintf(stderr, "USAGE: %s <host> <port> <numBytes>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char *host = argv[1];            /* Adresse IP du serveur */
	int port = better_atoi(argv[2]);        /* Port du service */
	size_t numBytes = better_atoll(argv[3]); /* Nombre de bytes aléatoires demandés */


	/* Connection */
	int sock = makeSocket( host, port );


	/* Envoie de la requête */
	if( write( sock, &numBytes, sizeof( numBytes ) ) < (long int) sizeof( numBytes ) ) {
		die( "Cannot send the filename to retrieve" );
	}


	/* Reception de la réponse */
	char *data = (char*) malloc( numBytes );/* Buffer de reception */

	ssize_t n=0;
	size_t rcvd=0; /* Bytes reçus */
	while( rcvd < numBytes ) {
		n = read( sock, data + rcvd, numBytes - rcvd );
		if( n  < 0 ) {
			die( "Cannot receive data" );
		}
		rcvd += n;
		printf( "Received %ld bytes.\n", n );
	}

	/* Afficher le résultats en hexadecimal */

	printf( "Received: " );
	for( size_t i = 0 ; i < numBytes ; i++ ) {
		printf( "%02x", data[i] & 0xff );
	}
	printf("\n"); 

	/* Libération des resources */
	free( data );
	close( sock );
	
	exit(EXIT_SUCCESS);
}
