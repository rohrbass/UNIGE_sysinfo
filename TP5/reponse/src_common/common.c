#include <stdio.h>
#include <stdlib.h>

void die(char *issue) {
	perror(issue); 
	exit(EXIT_FAILURE); 
}

int better_atoi( const char* str ) {
	// TODO : error handling, atoi is too dumb
	return atoi( str );
}

long long int better_atoll( const char* str ) {
	// TODO : error handling, atoi is too dumb
	return atoll( str );
}
