#pragma once

#define min(x,y) (((x)<(y))?(x):(y))

#define UNUSED(x) (void)(x)

void die(char *issue);

// atoi that crashes on error
int better_atoi( const char* str );
long long int better_atoll( const char* str );
