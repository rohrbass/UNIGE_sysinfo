#include "constants.h"

#include <stdlib.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
// #include <unistd.h> // for the sleep command
#include <time.h>

void cook_pizza(){
	// int random_nb = rand() % 400000;
	int random_nb = DELAY_RANDOM_COOK * (rand() * 1. / RAND_MAX);
	usleep(random_nb); // rends le nombre de sec qu'il reste à attendre si on est interrompu par un signal
	return;
}

sem_t * open_semaphore(char * name, int init){
	// ouvrir le fichier
	sem_t * sema;
	if((sema = sem_open(name, O_RDWR|O_CREAT, 0664, init))==SEM_FAILED){
		perror("cook : shm_open : ");
		return NULL;
	}
	return sema;
}
/*
void increment_semaphore(sem_t * sema){
	if(sem_post(sema)==-1){
		perror("cook : sem_post : ");
	}
}*/ 

int main(){
	// set random seed
	srandom(time(NULL));

	// --- INITIALISATION 
	// create semaphore
	sem_t *s_pizza_ready = open_semaphore("ready", 0);
	sem_t *s_pizza_slot  = open_semaphore("pizza", PIZZA_SLOT_COUNT+1);
	sem_t *s_lock        = open_semaphore("lock", 1);
	if( (s_pizza_ready==NULL) | (s_pizza_slot==NULL) | (s_lock==NULL) ){
		return -1;
	} 

	// --- CORE
	for(int i = 0; i<PIZZA_MAX; i++){

		// wait for a free slot and take it
		sem_wait(s_pizza_slot);

		// cook a pizza
		cook_pizza();

		// lock the shelf
		sem_wait(s_lock);

			// put the pizza on the shelf
			sem_post(s_pizza_ready);

			// report pizzas status
			int nbPizza;
			sem_getvalue(s_pizza_ready, &nbPizza);
			printf("\x1B[35m");
			printf("cook:   pizza-%02d; %d pizzas on the shelf.\n", i, nbPizza);
			printf("\x1B[0m");

		// unlock the shelf
		sem_post(s_lock);


	}
	// notify the user, I'm going home from work.
	sem_wait(s_lock);// don't let every one talk at the same time
		printf("\x1B[35m");
		printf("OK bye bye ! All pizzas done.\n");
		printf("\x1B[0m");
	sem_post(s_lock);

	// --- FREE RESSOURCES 

	// clean up my semaphores
	if(sem_unlink("ready")==-1){
		perror("cook : sem_unlink :");
		return -1;
	}
	if(sem_unlink("pizza")==-1){
		perror("cook : sem_unlink :");
		return -1;
	}
	if(sem_unlink("lock")==-1){
		perror("cook : sem_unlink :");
		return -1;
	}

	// wait that the waiter is done, then close the semaphore 
	if(sem_close(s_pizza_ready)==-1){
		perror("cook : sem_close :");
		return -1;
	}
	if(sem_close(s_pizza_slot)==-1){
		perror("cook : sem_close :");
		return -1;
	}
	if(sem_close(s_lock)==-1){
		perror("cook : sem_close :");
		return -1;
	}

	return 0;

}