#include "constants.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <time.h>

void serve_pizza(){
	int random_nb = DELAY_RANDOM_WAITER * (rand() * 1. / RAND_MAX);
	usleep(random_nb); // rends le nombre de sec qu'il reste à attendre si on est interrompu par un signal
	return;
}

/*
void increment_semaphore(sem_t * sema){
	if(sem_post(sema)==-1){
		perror("waiter : sem_post : ");
	}
}*/

sem_t * open_semaphore(char * name){
	sem_t * sema;
	if((sema = sem_open(name, O_RDWR))==SEM_FAILED){
		perror("waiter : shm_open : ");
		return NULL;
	}
	return sema;
}


int main(){
	// set the random seed
	srandom(1013+time(NULL));

	// --- INITIALISE
	// open the semaphores created by the cook
	sem_t *s_pizza_ready = NULL;
	sem_t *s_pizza_slot = NULL;
	sem_t *s_lock = NULL;

	while( (s_pizza_ready==NULL) || (s_pizza_slot==NULL) || (s_lock==NULL) ) {
		printf("waiting for cook to launch\n");
		// usleep(2000);
		s_pizza_ready = open_semaphore("ready");
		s_pizza_slot  = open_semaphore("pizza");
		s_lock        = open_semaphore("lock");
		// return -1;
	}


	// --- CORE
	for(int i = 0; i<PIZZA_MAX; i++) {

		// wait for a pizza and take it
		sem_wait(s_pizza_ready);

		// bring the pizza to the client
		serve_pizza();

		// lock the shelf
		sem_wait(s_lock);

			// free up a pizza from the shelf
			sem_post(s_pizza_slot);

			// report pizzas status
			int nbPizza;
			sem_getvalue(s_pizza_ready, &nbPizza);
			printf("\x1B[31m");
			printf("Waiter: pizza-%02d; %d pizzas on the shelf. \n", i, nbPizza);
			printf("\x1B[0m");

		// unlock the shelf
		sem_post(s_lock);

	}

	// FREE RESSOURCES

	if(sem_close(s_pizza_ready)==-1) {
		perror("cook : sem_close :");
		return -1;
	}
	if(sem_close(s_pizza_slot)==-1) {
		perror("cook : sem_close :");
		return -1;
	}
	if(sem_close(s_lock)==-1) {
		perror("cook : sem_close :");
		return -1;
	}

	return 0;

}